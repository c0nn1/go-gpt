package gogpt

type CreateThreadRequest struct {
	Messages []ThreadStartMessage `json:"messages,omitempty"`
	Metadata Metadata             `json:"metadata,omitempty"`
}

func NewCreateThreadRequest() *CreateThreadRequest {
	return &CreateThreadRequest{}
}
