package gogpt

import (
	"context"
	"encoding/json"
	"io"
	"net/http"
	"net/url"
)

type RunStepsAPIService service

type GetRunStepAPIRequest struct {
	ctx      context.Context
	threadID ThreadID
	runID    RunID
	stepID   RunStepID
	API      *RunStepsAPIService
}

func (e *RunStepsAPIService) Get(ctx context.Context, threadID ThreadID, runID RunID, stepID RunStepID) GetRunStepAPIRequest {
	return GetRunStepAPIRequest{
		ctx:      ctx,
		threadID: threadID,
		runID:    runID,
		stepID:   stepID,
		API:      e,
	}
}

func (r GetRunStepAPIRequest) Execute() (*RunStep, *http.Response, error) {
	return r.API.GetExecute(r)
}

func (e *RunStepsAPIService) GetExecute(r GetRunStepAPIRequest) (*RunStep, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "RunStepsAPIService.Get")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "threads", string(r.threadID), "runs", string(r.runID), "steps", string(r.stepID))
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeJSON().AddAssistantBeta()

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodGet, nil, headerParams, nil, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, resp, nil
	}

	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp, nil
	}

	if resp.StatusCode >= 300 {
		return nil, resp, NewOpenAIAPIError(respBody, resp.Status, nil)
	}

	returnValue := new(RunStep)
	if err = json.Unmarshal(respBody, returnValue); err != nil {
		return returnValue, resp, NewOpenAIAPIError(respBody, err.Error(), nil)
	}

	return returnValue, resp, nil
}

type ListRunStepsAPIRequest struct {
	ctx      context.Context
	threadID ThreadID
	runID    RunID
	stepID   RunStepID
	page     *PageRequest
	API      *RunStepsAPIService
}

func (e *RunStepsAPIService) List(ctx context.Context, threadID ThreadID, runID RunID, stepID RunStepID) ListRunStepsAPIRequest {
	return ListRunStepsAPIRequest{
		ctx:      ctx,
		threadID: threadID,
		runID:    runID,
		stepID:   stepID,
		API:      e,
	}
}

func (r ListRunStepsAPIRequest) Page(p *PageRequest) ListRunStepsAPIRequest {
	r.page = p
	return r
}

func (r ListRunStepsAPIRequest) Execute() (*PagedRunSteps, *http.Response, error) {
	return r.API.ListExecute(r)
}

func (e *RunStepsAPIService) ListExecute(r ListRunStepsAPIRequest) (*PagedRunSteps, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "RunStepsAPIService.List")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "threads", string(r.threadID), "runs", string(r.runID), "steps")
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeJSON().AddAssistantBeta()

	queryParams := url.Values{}
	r.page.Query(queryParams)

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodGet, nil, headerParams, queryParams, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, resp, nil
	}

	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp, nil
	}

	if resp.StatusCode >= 300 {
		return nil, resp, NewOpenAIAPIError(respBody, resp.Status, nil)
	}

	returnValue := new(PagedRunSteps)
	if err = json.Unmarshal(respBody, returnValue); err != nil {
		return returnValue, resp, NewOpenAIAPIError(respBody, err.Error(), nil)
	}

	return returnValue, resp, nil
}
