package gogpt

type CreateImageRequest struct {
	Prompt         string               `json:"prompt"`
	Model          Model                `json:"model,omitempty"`
	N              *int                 `json:"n,omitempty"`
	Quality        ImageQuality         `json:"quality,omitempty"`
	ResponseFormat *ImageResponseFormat `json:"response_format,omitempty"`
	Size           *ImageSize           `json:"size,omitempty"`
	Style          *ImageStyle          `json:"style,omitempty"`
	User           string               `json:"user,omitempty"`
}

func NewCreateImageRequest(prompt string) *CreateImageRequest {
	return &CreateImageRequest{Prompt: prompt}
}

type CreateImageRequestBuilder struct {
	req CreateImageRequest
}

func NewCreateImageRequestBuilder(prompt string) *CreateImageRequestBuilder {
	return &CreateImageRequestBuilder{req: CreateImageRequest{Prompt: prompt}}
}

func (b *CreateImageRequestBuilder) Build() *CreateImageRequest {
	return &b.req
}

func (b *CreateImageRequestBuilder) WithModel(m Model) *CreateImageRequestBuilder {
	b.req.Model = m
	return b
}

func (b *CreateImageRequestBuilder) WithN(n int) *CreateImageRequestBuilder {
	b.req.N = &n
	return b
}

func (b *CreateImageRequestBuilder) WithQuality(q ImageQuality) *CreateImageRequestBuilder {
	b.req.Quality = q
	return b
}

func (b *CreateImageRequestBuilder) WithResponseFormat(rf ImageResponseFormat) *CreateImageRequestBuilder {
	b.req.ResponseFormat = &rf
	return b
}

func (b *CreateImageRequestBuilder) WithSize(s ImageSize) *CreateImageRequestBuilder {
	b.req.Size = &s
	return b
}

func (b *CreateImageRequestBuilder) WithStyle(s ImageStyle) *CreateImageRequestBuilder {
	b.req.Style = &s
	return b
}

func (b *CreateImageRequestBuilder) WithUser(u string) *CreateImageRequestBuilder {
	b.req.User = u
	return b
}
