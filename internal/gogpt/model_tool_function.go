package gogpt

type ToolFunction struct {
	// A description of what the function does, used by the model to choose when and how to call the function.
	Description string `json:"description,omitempty"`
	// The name of the function to be called. Must be a-z, A-Z, 0-9, or contain underscores and dashes, with a maximum length of 64.
	Name string `json:"name"`
	// The parameters the functions accepts, described as a JSON Schema object. To describe a function that accepts no parameters, provide the value {"type": "object", "properties": {}}.
	Parameters ToolFunctionParameters `json:"parameters"`
}

type ToolFunctionParameters struct {
	Type       string                 `json:"type,omitempty"`
	Properties ToolFunctionProperties `json:"properties,omitempty"`
	Required   []string               `json:"required,omitempty"`
}

type ToolFunctionProperties map[string]ToolFunctionPropertyBody

type ToolFunctionPropertyBody struct {
	Type        string   `json:"type,omitempty"`
	Description string   `json:"description,omitempty"`
	Enum        []string `json:"enum,omitempty"`
}
