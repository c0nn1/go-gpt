package gogpt

type ToolChoice string

const (
	ToolChoiceNone ToolChoice = "none"
	ToolChoiceAuto ToolChoice = "auto"
)
