package gogpt

type SubmitToolOutputsRequest struct {
	ToolOutputs []ToolOutput `json:"tool_outputs"`
}

func NewSubmitToolOutputsRequest(toolOutputs []ToolOutput) *SubmitToolOutputsRequest {
	return &SubmitToolOutputsRequest{ToolOutputs: toolOutputs}
}
