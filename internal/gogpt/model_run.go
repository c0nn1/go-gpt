package gogpt

type RunID string

type Run struct {
	ID             RunID           `json:"id"`
	Object         string          `json:"object"`
	CreatedAt      int             `json:"created_at"`
	ThreadID       ThreadID        `json:"thread_id"`
	AssistantID    AssistantID     `json:"assistant_id"`
	Status         RunStatus       `json:"status"`
	RequiredAction *RequiredAction `json:"required_action"`
	LastError      *RunError       `json:"last_error"`
	ExpiresAt      int             `json:"expires_at"`
	StartedAt      *int            `json:"started_at"`
	CancelledAt    *int            `json:"cancelled_at"`
	FailedAt       *int            `json:"failed_at"`
	CompletedAt    *int            `json:"completed_at"`
	Model          string          `json:"model"`
	Instructions   string          `json:"instructions"`
	Tools          *RunTools       `json:"tools"`
	FileIDs        []string        `json:"file_ids"`
	Metadata       Metadata        `json:"metadata"`
}

type RequiredAction struct {
	Type              string            `json:"type"`
	SubmitToolOutputs SubmitToolOutputs `json:"submit_tool_outputs"`
}

type SubmitToolOutputs struct {
	ToolCalls []ToolCall `json:"tool_calls"`
}

type RunError struct {
	Code    string `json:"code"`
	Message string `json:"message"`
}

type RunStatus string

const (
	RunStatusQueued         RunStatus = "queued"
	RunStatusInProgress     RunStatus = "in_progress"
	RunStatusRequiresAction RunStatus = "requires_action"
	RunStatusCancelling     RunStatus = "cancelling"
	RunStatusCancelled      RunStatus = "cancelled"
	RunStatusFailed         RunStatus = "failed"
	RunStatusCompleted      RunStatus = "completed"
	RunStatusExpired        RunStatus = "expired"
)
