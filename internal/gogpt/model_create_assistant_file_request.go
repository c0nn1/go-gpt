package gogpt

type CreateAssistantFileRequest struct {
	FileID string `json:"file_id"`
}

func NewCreateAssistantFileRequest(fileID string) *CreateAssistantFileRequest {
	return &CreateAssistantFileRequest{FileID: fileID}
}
