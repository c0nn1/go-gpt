package gogpt

type CreateTranslationRequest struct {
	FilePath       string
	Model          Model
	Prompt         string
	ResponseFormat TranscriptionResponseFormat
	Temperature    float64
}

func NewCreateTranslationRequest(filePath string, model Model) *CreateTranslationRequest {
	return &CreateTranslationRequest{
		FilePath:       filePath,
		Model:          model,
		ResponseFormat: TranscriptionFormatJSON,
		Temperature:    0,
	}
}
