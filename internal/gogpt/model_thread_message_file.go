package gogpt

type ThreadMessageFileID string

type ThreadMessageFile struct {
	ID        ThreadMessageFileID `json:"id"`
	Object    string              `json:"object"`
	CreatedAt int                 `json:"created_at"`
	MessageID ThreadMessageID     `json:"message_id"`
}
