package gogpt

type ToolCall struct {
	// The ID of the tool call.
	ID string `json:"id"`
	// The type of the tool. Currently, only function is supported.
	Type string `json:"type"`
	// The function that the model called.
	Function Function `json:"function"`
}
