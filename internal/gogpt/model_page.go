package gogpt

type Page struct {
	Object  string `json:"object"`
	FirstID string `json:"first_id"`
	LastID  string `json:"last_id"`
	HasMore bool   `json:"has_more"`
}

type PagedThreadMessages struct {
	Page
	Data []ThreadMessage `json:"data"`
}

type PagedThreadMessageFiles struct {
	Page
	Data []ThreadMessageFile `json:"data"`
}

type PagedAssistants struct {
	Page
	Data []Assistant `json:"data"`
}

type PagedAssistantFiles struct {
	Page
	Data []AssistantFile `json:"data"`
}

type PagedFiles struct {
	Page
	Data []File `json:"data"`
}

type PagedRuns struct {
	Page
	Data []Run `json:"data"`
}

type PagedRunSteps struct {
	Page
	Data []RunStep `json:"data"`
}
