package gogpt

type CreateImageEditResponse struct {
	Created int     `json:"created"`
	Data    []Image `json:"data"`
}
