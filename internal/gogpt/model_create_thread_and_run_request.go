package gogpt

type CreateThreadAndRunRequest struct {
	AssistantID  AssistantID          `json:"assistant_id"`
	Thread       *CreateThreadRequest `json:"thread,omitempty"`
	Model        *Model               `json:"model,omitempty"`
	Instructions *string              `json:"instructions,omitempty"`
	Tools        *[]RunTool           `json:"tools,omitempty"`
	Metadata     Metadata             `json:"metadata,omitempty"`
}

func NewCreateThreadAndRunRequest(assistantID AssistantID, thread *CreateThreadRequest, model *Model, instructions *string, tools *[]RunTool, metadata Metadata) *CreateThreadAndRunRequest {
	return &CreateThreadAndRunRequest{
		AssistantID:  assistantID,
		Thread:       thread,
		Model:        model,
		Instructions: instructions,
		Tools:        tools,
		Metadata:     metadata,
	}
}
