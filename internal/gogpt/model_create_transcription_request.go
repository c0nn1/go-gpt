package gogpt

type CreateTranscriptionRequest struct {
	FilePath       string
	Model          Model
	Language       string
	Prompt         string
	ResponseFormat TranscriptionResponseFormat
	Temperature    float64
}

func NewCreateTranscriptionRequest(filePath string, model Model) *CreateTranscriptionRequest {
	return &CreateTranscriptionRequest{
		FilePath:       filePath,
		Model:          model,
		ResponseFormat: TranscriptionFormatJSON,
		Temperature:    0,
	}
}
