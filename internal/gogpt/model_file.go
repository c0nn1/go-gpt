package gogpt

type FileID string

type Purpose string

const (
	PurposeFineTune         Purpose = "fine-tune"
	PurposeFineTuneResult   Purpose = "fine-tune-result"
	PurposeAssistants       Purpose = "assistants"
	PurposeAssistantsOutput Purpose = "assistants_output"
)

type File struct {
	ID        FileID  `json:"id"`
	Bytes     int     `json:"bytes"`
	CreatedAt int     `json:"created_at"`
	Filename  string  `json:"filename"`
	Object    string  `json:"object"`
	Purpose   Purpose `json:"purpose"`
}
