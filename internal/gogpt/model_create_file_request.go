package gogpt

type CreateFileRequest struct {
	FilePath string
	Purpose  Purpose
}

func NewCreateFileRequest(filePath string, purpose Purpose) *CreateFileRequest {
	return &CreateFileRequest{FilePath: filePath, Purpose: purpose}
}
