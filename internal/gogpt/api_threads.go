package gogpt

import (
	"context"
	"encoding/json"
	"io"
	"net/http"
	"net/url"
)

// To list and delete Threads take a look here: https://community.openai.com/t/list-and-delete-all-threads/505823
// Use Bearer sess_

type ThreadsAPIService service

type CreateThreadAPIRequest struct {
	ctx           context.Context
	createRequest *CreateThreadRequest
	API           *ThreadsAPIService
}

func (e *ThreadsAPIService) Create(ctx context.Context) CreateThreadAPIRequest {
	return CreateThreadAPIRequest{
		ctx: ctx,
		API: e,
	}
}

func (r CreateThreadAPIRequest) CreateRequest(req *CreateThreadRequest) CreateThreadAPIRequest {
	r.createRequest = req
	return r
}

func (r CreateThreadAPIRequest) Execute() (*Thread, *http.Response, error) {
	return r.API.CreateExecute(r)
}

func (e *ThreadsAPIService) CreateExecute(r CreateThreadAPIRequest) (*Thread, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "ThreadsAPIService.Create")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "threads")
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeJSON().AddAssistantBeta()

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodPost, r.createRequest, headerParams, nil, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, resp, nil
	}

	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp, nil
	}

	if resp.StatusCode >= 300 {
		return nil, resp, NewOpenAIAPIError(respBody, resp.Status, nil)
	}

	returnValue := new(Thread)
	if err = json.Unmarshal(respBody, returnValue); err != nil {
		return returnValue, resp, NewOpenAIAPIError(respBody, err.Error(), nil)
	}

	return returnValue, resp, nil
}

type GetThreadAPIRequest struct {
	ctx context.Context
	id  ThreadID
	API *ThreadsAPIService
}

func (e *ThreadsAPIService) Get(ctx context.Context, id ThreadID) GetThreadAPIRequest {
	return GetThreadAPIRequest{
		ctx: ctx,
		id:  id,
		API: e,
	}
}

func (r GetThreadAPIRequest) Execute() (*Thread, *http.Response, error) {
	return r.API.GetExecute(r)
}

func (e *ThreadsAPIService) GetExecute(r GetThreadAPIRequest) (*Thread, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "ThreadsAPIService.Get")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "threads", string(r.id))
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeJSON().AddAssistantBeta()

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodGet, nil, headerParams, nil, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, resp, nil
	}

	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp, nil
	}

	if resp.StatusCode >= 300 {
		return nil, resp, NewOpenAIAPIError(respBody, resp.Status, nil)
	}

	returnValue := new(Thread)
	if err = json.Unmarshal(respBody, returnValue); err != nil {
		return returnValue, resp, NewOpenAIAPIError(respBody, err.Error(), nil)
	}

	return returnValue, resp, nil
}
