package gogpt

import (
	"bufio"
	"context"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"net/url"
	"strings"
)

type ChatCompletionAPIService service

type SendChatCompletionAPIRequest struct {
	ctx         context.Context
	sendRequest *ChatCompletionRequest
	API         *ChatCompletionAPIService
}

func (e *ChatCompletionAPIService) Send(ctx context.Context) SendChatCompletionAPIRequest {
	return SendChatCompletionAPIRequest{
		ctx: ctx,
		API: e,
	}
}

func (r SendChatCompletionAPIRequest) SendRequest(req *ChatCompletionRequest) SendChatCompletionAPIRequest {
	r.sendRequest = req
	return r
}

func (r SendChatCompletionAPIRequest) Execute() (*ChatCompletionResponse, *http.Response, error) {
	return r.API.SendExecute(r)
}

func (e *ChatCompletionAPIService) SendExecute(r SendChatCompletionAPIRequest) (*ChatCompletionResponse, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "ChatCompletionAPIService.Send")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "chat", "completions")
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeJSON()

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodPost, r.sendRequest, headerParams, nil, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, resp, nil
	}

	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp, nil
	}

	if resp.StatusCode >= 300 {
		return nil, resp, NewOpenAIAPIError(respBody, resp.Status, nil)
	}

	returnValue := new(ChatCompletionResponse)

	if err = json.Unmarshal(respBody, returnValue); err != nil {
		return returnValue, resp, NewOpenAIAPIError(respBody, err.Error(), nil)
	}

	return returnValue, resp, nil
}

func (r SendChatCompletionAPIRequest) Stream() (<-chan *ChatCompletionChunkResponse, *http.Response, error) {
	return r.API.StreamExecute(r)
}

func (e *ChatCompletionAPIService) StreamExecute(r SendChatCompletionAPIRequest) (<-chan *ChatCompletionChunkResponse, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "ChatCompletionAPIService.Send")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "chat", "completions")
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeJSON()

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodPost, r.sendRequest, headerParams, nil, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, resp, nil
	}

	if resp.StatusCode >= 300 {
		defer resp.Body.Close()
		body, err := io.ReadAll(resp.Body)
		if err != nil {
			return nil, resp, err
		}
		return nil, resp, NewOpenAIAPIError(body, resp.Status, nil)
	}

	ch := make(chan *ChatCompletionChunkResponse, 10)

	go func(body io.ReadCloser, ch chan<- *ChatCompletionChunkResponse) {
		defer body.Close()
		defer close(ch)

		e.readChunks(body, ch)
	}(resp.Body, ch)

	return ch, resp, nil
}

func (e *ChatCompletionAPIService) readChunks(body io.ReadCloser, ch chan<- *ChatCompletionChunkResponse) {
	scanner := bufio.NewScanner(body)

	const dataPrefix = "data: "
	const doneMessage = "[DONE]"

	for scanner.Scan() {
		line := scanner.Text()

		if !strings.HasPrefix(line, dataPrefix) {
			continue
		}

		payload := strings.TrimPrefix(line, dataPrefix)

		if payload == doneMessage {
			break
		}

		chunk := new(ChatCompletionChunkResponse)
		if err := json.Unmarshal([]byte(payload), chunk); err != nil {
			log.Println(err)
			continue
		}

		ch <- chunk
	}
}
