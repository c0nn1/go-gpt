package gogpt

type CreateImageResponse struct {
	Created int     `json:"created"`
	Data    []Image `json:"data"`
}
