package gogpt

type Usage struct {
	// Number of tokens in the generated completion.
	CompletionTokens int `json:"completion_tokens"`
	// Number of tokens in the prompt.
	PromptTokens int `json:"prompt_tokens"`
	// Total number of tokens used in the request (prompt + completion).
	TotalTokens int `json:"total_tokens"`
}
