package gogpt

import (
	"encoding/json"
	"fmt"
	"strings"
)

const (
	RunToolTypeFunction        = "function"
	RunToolTypeCodeInterpreter = "code_interpreter"
	RunToolTypeRetrieval       = "retrieval"
)

type RunTools []RunTool

func (rt *RunTools) UnmarshalJSON(data []byte) error {
	var err error

	raw := new([]json.RawMessage)
	if err = json.Unmarshal(data, raw); err != nil {
		return err
	}

	tools := make(RunTools, len(*raw))
	for i, r := range *raw {
		var fields map[string]json.RawMessage
		if err = json.Unmarshal(r, &fields); err != nil {
			return err
		}

		var runTool RunTool
		toolType := strings.Trim(string(fields["type"]), "\"")
		if toolType == RunToolTypeFunction {
			runTool = new(FunctionTool)
		} else if toolType == RunToolTypeRetrieval {
			runTool = new(RetrievalTool)
		} else if toolType == RunToolTypeCodeInterpreter {
			runTool = new(CodeInterpreterTool)
		} else {
			return fmt.Errorf("toolType \"%s\" is not valid", toolType)
		}

		if err = json.Unmarshal(r, &runTool); err != nil {
			return err
		}

		tools[i] = runTool
	}

	*rt = tools
	return nil
}

type RunTool interface {
	Type() string
}

type CodeInterpreterTool struct {
	T string `json:"type"`
}

func (t CodeInterpreterTool) Type() string {
	return t.T
}

type RetrievalTool struct {
	T string `json:"type"`
}

func NewRetrievalTool() *RetrievalTool {
	return &RetrievalTool{T: "retrieval"}
}

func (t RetrievalTool) Type() string {
	return t.T
}

type FunctionTool struct {
	T        string       `json:"type"`
	Function ToolFunction `json:"function"`
}

func NewFunctionTool(tf ToolFunction) *FunctionTool {
	return &FunctionTool{
		T:        "function",
		Function: tf,
	}
}

func (t FunctionTool) Type() string {
	return t.T
}
