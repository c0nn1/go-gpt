package gogpt

import (
	"context"
	"encoding/json"
	"io"
	"net/http"
	"net/url"
)

type ThreadMessageFilesAPIService service

type GetThreadMessageFilesAPIRequest struct {
	ctx       context.Context
	threadID  ThreadID
	messageID ThreadMessageID
	fileID    ThreadMessageFileID
	API       *ThreadMessageFilesAPIService
}

func (e *ThreadMessageFilesAPIService) Get(ctx context.Context, threadID ThreadID, messageID ThreadMessageID, fileID ThreadMessageFileID) GetThreadMessageFilesAPIRequest {
	return GetThreadMessageFilesAPIRequest{
		ctx:       ctx,
		threadID:  threadID,
		messageID: messageID,
		fileID:    fileID,
		API:       e,
	}
}

func (r GetThreadMessageFilesAPIRequest) Execute() (*ThreadMessageFile, *http.Response, error) {
	return r.API.GetExecute(r)
}

func (e *ThreadMessageFilesAPIService) GetExecute(r GetThreadMessageFilesAPIRequest) (*ThreadMessageFile, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "ThreadMessageFilesAPIService.Get")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "threads", string(r.threadID), "messages", string(r.messageID), "files", string(r.fileID))
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeJSON().AddAssistantBeta()

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodGet, nil, headerParams, nil, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, resp, nil
	}

	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp, nil
	}

	if resp.StatusCode >= 300 {
		return nil, resp, NewOpenAIAPIError(respBody, resp.Status, nil)
	}

	returnValue := new(ThreadMessageFile)
	if err = json.Unmarshal(respBody, returnValue); err != nil {
		return returnValue, resp, NewOpenAIAPIError(respBody, err.Error(), nil)
	}

	return returnValue, resp, nil
}

type ListThreadMessageFilesAPIRequest struct {
	ctx       context.Context
	threadID  ThreadID
	messageID ThreadMessageID
	page      *PageRequest
	API       *ThreadMessageFilesAPIService
}

func (e *ThreadMessageFilesAPIService) List(ctx context.Context, threadID ThreadID, messageID ThreadMessageID) ListThreadMessageFilesAPIRequest {
	return ListThreadMessageFilesAPIRequest{
		ctx:       ctx,
		threadID:  threadID,
		messageID: messageID,
		API:       e,
	}
}

func (r ListThreadMessageFilesAPIRequest) Page(p *PageRequest) ListThreadMessageFilesAPIRequest {
	r.page = p
	return r
}

func (r ListThreadMessageFilesAPIRequest) Execute() (*PagedThreadMessageFiles, *http.Response, error) {
	return r.API.ListExecute(r)
}

func (e *ThreadMessageFilesAPIService) ListExecute(r ListThreadMessageFilesAPIRequest) (*PagedThreadMessageFiles, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "ThreadMessageFilesAPIService.List")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "threads", string(r.threadID), "messages", string(r.messageID), "files")
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeJSON().AddAssistantBeta()

	queryParams := url.Values{}
	r.page.Query(queryParams)

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodGet, nil, headerParams, queryParams, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, resp, nil
	}

	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp, nil
	}

	if resp.StatusCode >= 300 {
		return nil, resp, NewOpenAIAPIError(respBody, resp.Status, nil)
	}

	returnValue := new(PagedThreadMessageFiles)
	if err = json.Unmarshal(respBody, returnValue); err != nil {
		return returnValue, resp, NewOpenAIAPIError(respBody, err.Error(), nil)
	}

	return returnValue, resp, nil
}
