package gogpt

type AssistantID string

type Assistant struct {
	ID           AssistantID `json:"id"`
	Object       string      `json:"object"`
	CreatedAt    int         `json:"created_at"`
	Name         *string     `json:"name"`
	Description  *string     `json:"description"`
	Model        Model       `json:"model"`
	Instructions *string     `json:"instructions"`
	Tools        *RunTools   `json:"tools"`
	FileIDs      []string    `json:"file_ids"`
	Metadata     Metadata    `json:"metadata"`
}
