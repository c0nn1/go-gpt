package gogpt

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"net/url"
)

type AudioAPIService service

type CreateSpeechAPIRequest struct {
	ctx           context.Context
	speechRequest *CreateSpeechRequest
	API           *AudioAPIService
}

func (e *AudioAPIService) CreateSpeech(ctx context.Context) CreateSpeechAPIRequest {
	return CreateSpeechAPIRequest{
		ctx: ctx,
		API: e,
	}
}

func (r CreateSpeechAPIRequest) CreateSpeechRequest(req *CreateSpeechRequest) CreateSpeechAPIRequest {
	r.speechRequest = req
	return r
}

func (r CreateSpeechAPIRequest) Execute() ([]byte, *http.Response, error) {
	return r.API.CreateSpeechExecute(r)
}

func (e *AudioAPIService) CreateSpeechExecute(r CreateSpeechAPIRequest) ([]byte, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "AudioAPIService.CreateSpeech")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "audio", "speech")
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeJSON()

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodPost, r.speechRequest, headerParams, nil, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, resp, nil
	}

	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp, nil
	}

	if resp.StatusCode >= 300 {
		return nil, resp, NewOpenAIAPIError(respBody, resp.Status, nil)
	}

	return respBody, resp, nil
}

type CreateTranscriptionAPIRequest struct {
	ctx                  context.Context
	transcriptionRequest *CreateTranscriptionRequest
	API                  *AudioAPIService
}

func (e *AudioAPIService) CreateTranscription(ctx context.Context) CreateTranscriptionAPIRequest {
	return CreateTranscriptionAPIRequest{
		ctx: ctx,
		API: e,
	}
}

func (r CreateTranscriptionAPIRequest) CreateTranscriptionRequest(req *CreateTranscriptionRequest) CreateTranscriptionAPIRequest {
	r.transcriptionRequest = req
	return r
}

func (r CreateTranscriptionAPIRequest) Execute() (*TranscribedTextResult, *http.Response, error) {
	return r.API.CreateTranscriptionExecute(r)
}

func (e *AudioAPIService) CreateTranscriptionExecute(r CreateTranscriptionAPIRequest) (*TranscribedTextResult, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "AudioAPIService.CreateTranscription")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "audio", "transcriptions")
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeFormData()

	formParams := url.Values{}
	formParams.Set("@file", r.transcriptionRequest.FilePath)
	formParams.Set("model", string(r.transcriptionRequest.Model))
	formParams.Set("temperature", fmt.Sprintf("%v", r.transcriptionRequest.Temperature))
	if len(r.transcriptionRequest.Language) > 0 {
		formParams.Set("language", r.transcriptionRequest.Language)
	}

	if len(r.transcriptionRequest.Prompt) > 0 {
		formParams.Set("prompt", r.transcriptionRequest.Prompt)
	}

	if len(r.transcriptionRequest.ResponseFormat) > 0 {
		formParams.Set("response_format", string(r.transcriptionRequest.ResponseFormat))
	}

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodPost, nil, headerParams, nil, formParams, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, resp, err
	}

	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp, err
	}

	if resp.StatusCode >= 300 {
		return nil, resp, NewOpenAIAPIError(respBody, resp.Status, nil)
	}

	returnValue := &TranscribedTextResult{
		Text: string(respBody),
	}

	return returnValue, resp, nil
}

type CreateTranslationAPIRequest struct {
	ctx                context.Context
	translationRequest *CreateTranslationRequest
	API                *AudioAPIService
}

func (e *AudioAPIService) CreateTranslation(ctx context.Context) CreateTranslationAPIRequest {
	return CreateTranslationAPIRequest{
		ctx: ctx,
		API: e,
	}
}

func (r CreateTranslationAPIRequest) CreateTranslationRequest(req *CreateTranslationRequest) CreateTranslationAPIRequest {
	r.translationRequest = req
	return r
}

func (r CreateTranslationAPIRequest) Execute() (*TranscribedTextResult, *http.Response, error) {
	return r.API.CreateTranslationExecute(r)
}

func (e *AudioAPIService) CreateTranslationExecute(r CreateTranslationAPIRequest) (*TranscribedTextResult, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "AudioAPIService.CreateTranslation")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "audio", "translations")
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeFormData()

	formParams := url.Values{}
	formParams.Set("@file", r.translationRequest.FilePath)
	formParams.Set("model", string(r.translationRequest.Model))
	formParams.Set("temperature", fmt.Sprintf("%v", r.translationRequest.Temperature))
	if len(r.translationRequest.Prompt) > 0 {
		formParams.Set("prompt", r.translationRequest.Prompt)
	}

	if len(r.translationRequest.ResponseFormat) > 0 {
		formParams.Set("response_format", string(r.translationRequest.ResponseFormat))
	}

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodPost, nil, headerParams, nil, formParams, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, resp, nil
	}

	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp, nil
	}

	if resp.StatusCode >= 300 {
		return nil, resp, NewOpenAIAPIError(respBody, resp.Status, nil)
	}

	returnValue := &TranscribedTextResult{
		Text: string(respBody),
	}

	return returnValue, resp, nil
}
