package gogpt

import (
	"context"
	"encoding/json"
	"io"
	"net/http"
	"net/url"
)

type RunsAPIService service

type CreateRunAPIRequest struct {
	ctx           context.Context
	threadID      ThreadID
	createRequest *CreateRunRequest
	API           *RunsAPIService
}

func (e *RunsAPIService) Create(ctx context.Context, threadID ThreadID) CreateRunAPIRequest {
	return CreateRunAPIRequest{
		ctx:      ctx,
		threadID: threadID,
		API:      e,
	}
}

func (r CreateRunAPIRequest) CreateRequest(req *CreateRunRequest) CreateRunAPIRequest {
	r.createRequest = req
	return r
}

func (r CreateRunAPIRequest) Execute() (*Run, *http.Response, error) {
	return r.API.CreateExecute(r)
}

func (e *RunsAPIService) CreateExecute(r CreateRunAPIRequest) (*Run, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "RunsAPIService.Create")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "threads", string(r.threadID), "runs")
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeJSON().AddAssistantBeta()

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodPost, r.createRequest, headerParams, nil, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, resp, nil
	}

	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp, nil
	}

	if resp.StatusCode >= 300 {
		return nil, resp, NewOpenAIAPIError(respBody, resp.Status, nil)
	}

	returnValue := new(Run)
	if err = json.Unmarshal(respBody, returnValue); err != nil {
		return returnValue, resp, NewOpenAIAPIError(respBody, err.Error(), nil)
	}

	return returnValue, resp, nil
}

type GetRunAPIRequest struct {
	ctx      context.Context
	threadID ThreadID
	runID    RunID
	API      *RunsAPIService
}

func (e *RunsAPIService) Get(ctx context.Context, threadID ThreadID, runID RunID) GetRunAPIRequest {
	return GetRunAPIRequest{
		ctx:      ctx,
		threadID: threadID,
		runID:    runID,
		API:      e,
	}
}

func (r GetRunAPIRequest) Execute() (*Run, *http.Response, error) {
	return r.API.GetExecute(r)
}

func (e *RunsAPIService) GetExecute(r GetRunAPIRequest) (*Run, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "RunsAPIService.Get")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "threads", string(r.threadID), "runs", string(r.runID))
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeJSON().AddAssistantBeta()

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodGet, nil, headerParams, nil, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, resp, nil
	}

	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp, nil
	}

	if resp.StatusCode >= 300 {
		return nil, resp, NewOpenAIAPIError(respBody, resp.Status, nil)
	}

	returnValue := new(Run)
	if err = json.Unmarshal(respBody, returnValue); err != nil {
		return returnValue, resp, NewOpenAIAPIError(respBody, err.Error(), nil)
	}

	return returnValue, resp, nil
}

type ModifyRunAPIRequest struct {
	ctx           context.Context
	threadID      ThreadID
	runID         RunID
	modifyRequest *ModifyRunRequest
	API           *RunsAPIService
}

func (e *RunsAPIService) Modify(ctx context.Context, threadID ThreadID, runID RunID) ModifyRunAPIRequest {
	return ModifyRunAPIRequest{
		ctx:      ctx,
		threadID: threadID,
		runID:    runID,
		API:      e,
	}
}

func (r ModifyRunAPIRequest) ModifyRequest(req *ModifyRunRequest) ModifyRunAPIRequest {
	r.modifyRequest = req
	return r
}

func (r ModifyRunAPIRequest) Execute() (*Run, *http.Response, error) {
	return r.API.ModifyExecute(r)
}

func (e *RunsAPIService) ModifyExecute(r ModifyRunAPIRequest) (*Run, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "RunsAPIService.Modify")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "threads", string(r.threadID), "runs", string(r.runID))
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeJSON().AddAssistantBeta()

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodPost, nil, headerParams, nil, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, resp, nil
	}

	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp, nil
	}

	if resp.StatusCode >= 300 {
		return nil, resp, NewOpenAIAPIError(respBody, resp.Status, nil)
	}

	returnValue := new(Run)
	if err = json.Unmarshal(respBody, returnValue); err != nil {
		return returnValue, resp, NewOpenAIAPIError(respBody, err.Error(), nil)
	}

	return returnValue, resp, nil
}

type ListRunsAPIRequest struct {
	ctx      context.Context
	threadID ThreadID
	page     *PageRequest
	API      *RunsAPIService
}

func (e *RunsAPIService) List(ctx context.Context, threadID ThreadID) ListRunsAPIRequest {
	return ListRunsAPIRequest{
		ctx:      ctx,
		threadID: threadID,
		API:      e,
	}
}

func (r ListRunsAPIRequest) Page(p *PageRequest) ListRunsAPIRequest {
	r.page = p
	return r
}

func (r ListRunsAPIRequest) Execute() (*PagedRuns, *http.Response, error) {
	return r.API.ListExecute(r)
}

func (e *RunsAPIService) ListExecute(r ListRunsAPIRequest) (*PagedRuns, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "RunsAPIService.List")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "threads", string(r.threadID), "runs")
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeJSON().AddAssistantBeta()

	queryParams := url.Values{}
	r.page.Query(queryParams)

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodGet, nil, headerParams, queryParams, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, resp, nil
	}

	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp, nil
	}

	if resp.StatusCode >= 300 {
		return nil, resp, NewOpenAIAPIError(respBody, resp.Status, nil)
	}

	returnValue := new(PagedRuns)
	if err = json.Unmarshal(respBody, returnValue); err != nil {
		return returnValue, resp, NewOpenAIAPIError(respBody, err.Error(), nil)
	}

	return returnValue, resp, nil
}

type SubmitToolOutputsAPIRequest struct {
	ctx           context.Context
	threadID      ThreadID
	runID         RunID
	submitRequest *SubmitToolOutputsRequest
	API           *RunsAPIService
}

func (e *RunsAPIService) SubmitToolOutputs(ctx context.Context, threadID ThreadID, runID RunID) SubmitToolOutputsAPIRequest {
	return SubmitToolOutputsAPIRequest{
		ctx:      ctx,
		threadID: threadID,
		runID:    runID,
		API:      e,
	}
}

func (r SubmitToolOutputsAPIRequest) SubmitRequest(req *SubmitToolOutputsRequest) SubmitToolOutputsAPIRequest {
	r.submitRequest = req
	return r
}

func (r SubmitToolOutputsAPIRequest) Execute() (*Run, *http.Response, error) {
	return r.API.SubmitToolOutputsExecute(r)
}

func (e *RunsAPIService) SubmitToolOutputsExecute(r SubmitToolOutputsAPIRequest) (*Run, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "RunsAPIService.SubmitToolOutputs")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "threads", string(r.threadID), "runs", string(r.runID), "submit_tool_outputs")
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeJSON().AddAssistantBeta()

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodPost, r.submitRequest, headerParams, nil, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, resp, nil
	}

	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp, nil
	}

	if resp.StatusCode >= 300 {
		return nil, resp, NewOpenAIAPIError(respBody, resp.Status, nil)
	}

	returnValue := new(Run)
	if err = json.Unmarshal(respBody, returnValue); err != nil {
		return returnValue, resp, NewOpenAIAPIError(respBody, err.Error(), nil)
	}

	return returnValue, resp, nil
}

type CancelRunAPIRequest struct {
	ctx      context.Context
	threadID ThreadID
	runID    RunID
	API      *RunsAPIService
}

func (e *RunsAPIService) Cancel(ctx context.Context, threadID ThreadID, runID RunID) CancelRunAPIRequest {
	return CancelRunAPIRequest{
		ctx:      ctx,
		threadID: threadID,
		runID:    runID,
		API:      e,
	}
}

func (r CancelRunAPIRequest) Execute() (*Run, *http.Response, error) {
	return r.API.CancelExecute(r)
}

func (e *RunsAPIService) CancelExecute(r CancelRunAPIRequest) (*Run, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "RunsAPIService.Cancel")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "threads", string(r.threadID), "runs", string(r.runID), "cancel")
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeJSON().AddAssistantBeta()

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodPost, nil, headerParams, nil, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, resp, nil
	}

	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp, nil
	}

	if resp.StatusCode >= 300 {
		return nil, resp, NewOpenAIAPIError(respBody, resp.Status, nil)
	}

	returnValue := new(Run)
	if err = json.Unmarshal(respBody, returnValue); err != nil {
		return returnValue, resp, NewOpenAIAPIError(respBody, err.Error(), nil)
	}

	return returnValue, resp, nil
}

type CreateThreadAndRunAPIRequest struct {
	ctx                 context.Context
	createAndRunRequest *CreateThreadAndRunRequest
	API                 *RunsAPIService
}

func (e *RunsAPIService) CreateThreadAndRun(ctx context.Context) CreateThreadAndRunAPIRequest {
	return CreateThreadAndRunAPIRequest{
		ctx: ctx,
		API: e,
	}
}

func (r CreateThreadAndRunAPIRequest) CreateAndRunRequest(req *CreateThreadAndRunRequest) CreateThreadAndRunAPIRequest {
	r.createAndRunRequest = req
	return r
}

func (r CreateThreadAndRunAPIRequest) Execute() (*Run, *http.Response, error) {
	return r.API.CreateThreadAndRunExecute(r)
}

func (e *RunsAPIService) CreateThreadAndRunExecute(r CreateThreadAndRunAPIRequest) (*Run, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "RunsAPIService.CreateThreadAndRun")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "threads", "runs")
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeJSON().AddAssistantBeta()

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodPost, r.createAndRunRequest, headerParams, nil, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, resp, nil
	}

	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp, nil
	}

	if resp.StatusCode >= 300 {
		return nil, resp, NewOpenAIAPIError(respBody, resp.Status, nil)
	}

	returnValue := new(Run)
	if err = json.Unmarshal(respBody, returnValue); err != nil {
		return returnValue, resp, NewOpenAIAPIError(respBody, err.Error(), nil)
	}

	return returnValue, resp, nil
}
