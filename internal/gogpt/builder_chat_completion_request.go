package gogpt

import (
	"fmt"
)

const toolChoiceTypeFunction = "function"

type ChatCompletionRequestBuilder struct {
	request *ChatCompletionRequest
}

func NewChatCompletionRequestBuilder(m Model) *ChatCompletionRequestBuilder {
	return &ChatCompletionRequestBuilder{
		request: &ChatCompletionRequest{Model: m},
	}
}

func (b *ChatCompletionRequestBuilder) WithUser(u string) *ChatCompletionRequestBuilder {
	b.request.User = u
	return b
}

func (b *ChatCompletionRequestBuilder) WithToolChoiceSpecific(functionName string) *ChatCompletionRequestBuilder {
	b.request.ToolChoice = ObjectChatCompletionToolChoice{
		Type: toolChoiceTypeFunction,
		Function: ToolChoiceFunction{
			Name: functionName,
		},
	}
	return b
}

func (b *ChatCompletionRequestBuilder) WithToolChoicePredefined(t ToolChoice) *ChatCompletionRequestBuilder {
	b.request.ToolChoice = StringChatCompletionToolChoice{Value: t}
	return b
}

func (b *ChatCompletionRequestBuilder) AddTool(t ChatCompletionTool) *ChatCompletionRequestBuilder {
	if b.request.Tools == nil {
		b.request.Tools = make([]ChatCompletionTool, 0, 3)
	}

	b.request.Tools = append(b.request.Tools, t)
	return b
}

func (b *ChatCompletionRequestBuilder) WithTools(t []ChatCompletionTool) *ChatCompletionRequestBuilder {
	b.request.Tools = t
	return b
}

func (b *ChatCompletionRequestBuilder) WithTopP(t float64) *ChatCompletionRequestBuilder {
	b.request.TopP = &t
	return b
}

func (b *ChatCompletionRequestBuilder) WithTemperature(temp float64) *ChatCompletionRequestBuilder {
	b.request.Temperature = &temp
	return b
}

func (b *ChatCompletionRequestBuilder) WithStreaming(s bool) *ChatCompletionRequestBuilder {
	b.request.Stream = &s
	return b
}

func (b *ChatCompletionRequestBuilder) WithStopArray(s []string) *ChatCompletionRequestBuilder {
	b.request.Stop = ArrayChatCompletionStopper{Value: s}
	return b
}

func (b *ChatCompletionRequestBuilder) WithStopString(s string) *ChatCompletionRequestBuilder {
	b.request.Stop = StringChatCompletionStopper{Value: s}
	return b
}

func (b *ChatCompletionRequestBuilder) WithSeed(s int) *ChatCompletionRequestBuilder {
	b.request.Seed = &s
	return b
}

func (b *ChatCompletionRequestBuilder) WithResponseFormatJSON() *ChatCompletionRequestBuilder {
	return b.WithResponseFormat("json_object")
}

func (b *ChatCompletionRequestBuilder) WithResponseFormat(t ChatResponseFormat) *ChatCompletionRequestBuilder {
	b.request.ResponseFormat = &ChatCompletionResponseFormat{Type: &t}
	return b
}

func (b *ChatCompletionRequestBuilder) WithPresencePenalty(pp float64) *ChatCompletionRequestBuilder {
	b.request.PresencePenalty = &pp
	return b
}

func (b *ChatCompletionRequestBuilder) WithN(n int) *ChatCompletionRequestBuilder {
	b.request.MaxTokens = &n
	return b
}

func (b *ChatCompletionRequestBuilder) WithMaxTokens(mt int) *ChatCompletionRequestBuilder {
	b.request.MaxTokens = &mt
	return b
}

func (b *ChatCompletionRequestBuilder) WithLogitBias(m map[string]float64) *ChatCompletionRequestBuilder {
	b.request.LogitBias = m
	return b
}

func (b *ChatCompletionRequestBuilder) WithFrequencyPenalty(fp float64) *ChatCompletionRequestBuilder {
	b.request.FrequencyPenalty = &fp
	return b
}

func (b *ChatCompletionRequestBuilder) AddMessage(m ConversationMessage) *ChatCompletionRequestBuilder {
	if b.request.Messages == nil {
		b.request.Messages = make([]ConversationMessage, 0, 3)
	}

	b.request.Messages = append(b.request.Messages, m)
	return b
}

func (b *ChatCompletionRequestBuilder) WithMessages(m []ConversationMessage) *ChatCompletionRequestBuilder {
	b.request.Messages = m
	return b
}

func (b *ChatCompletionRequestBuilder) Build() (*ChatCompletionRequest, error) {
	if len(b.request.Model) == 0 {
		return nil, fmt.Errorf("model must not be empty")
	}

	if len(b.request.Messages) == 0 {
		return nil, fmt.Errorf("at least one message is required")
	}

	return b.request, nil
}
