package gogpt

import (
	"encoding/json"
	"fmt"
	"strings"
)

type RunStepID string

type RunStep struct {
	ID          RunStepID      `json:"id"`
	Object      string         `json:"object"`
	CreatedAt   int            `json:"created_at"`
	AssistantID AssistantID    `json:"assistant_id"`
	ThreadID    ThreadID       `json:"thread_id"`
	RunID       RunID          `json:"run_id"`
	Type        string         `json:"type"`
	StepDetails RunStepDetails `json:"step_details"`
	LastError   *RunError      `json:"last_error"`
	ExpiredAt   *int           `json:"expired_at"`
	CancelledAt *int           `json:"cancelled_at"`
	FailedAt    *int           `json:"failed_at"`
	CompletedAt *int           `json:"completed_at"`
	Metadata    Metadata       `json:"metadata"`
}

type RunStepDetails interface {
	Type() string
}

type MessageCreationStepDetail struct {
	T               string `json:"type"`
	MessageCreation struct {
		MessageID ThreadMessageID `json:"message_id"` // todo: is this a 'thread'-message-id?
	} `json:"message_creation"`
}

func (sd MessageCreationStepDetail) Type() string {
	return sd.T
}

type ToolCallsStepDetail struct {
	T         string     `json:"type"`
	ToolCalls []ToolCall `json:"tool_calls"`
}

func (sd ToolCallsStepDetail) Type() string {
	return sd.T
}

func (r *RunStep) UnmarshalJSON(data []byte) error {
	raw := make(map[string]json.RawMessage)

	var err error

	if err = json.Unmarshal(data, &raw); err != nil {
		return err
	}

	if err = unmarshalOptional(raw, "id", &r.ID); err != nil {
		return err
	}

	if err = unmarshalOptional(raw, "object", &r.Object); err != nil {
		return err
	}

	if err = unmarshalOptional(raw, "created_at", &r.CreatedAt); err != nil {
		return err
	}

	if err = unmarshalOptional(raw, "assistant_id", &r.AssistantID); err != nil {
		return err
	}

	if err = unmarshalOptional(raw, "thread_id", &r.ThreadID); err != nil {
		return err
	}

	if err = unmarshalOptional(raw, "run_id", &r.RunID); err != nil {
		return err
	}

	if err = unmarshalOptional(raw, "type", &r.Type); err != nil {
		return err
	}

	if err = unmarshalOptional(raw, "last_error", &r.LastError); err != nil {
		return err
	}

	if err = unmarshalOptional(raw, "expired_at", &r.ExpiredAt); err != nil {
		return err
	}

	if err = unmarshalOptional(raw, "cancelled_at", &r.CancelledAt); err != nil {
		return err
	}

	if err = unmarshalOptional(raw, "failed_at", &r.FailedAt); err != nil {
		return err
	}

	if err = unmarshalOptional(raw, "completed_at", &r.CompletedAt); err != nil {
		return err
	}

	if err = unmarshalOptional(raw, "metadata", &r.Metadata); err != nil {
		return err
	}

	rawStepDetail := make(map[string]json.RawMessage)
	if err = unmarshalOptional(raw, "step_details", &rawStepDetail); err != nil {
		return err
	}

	typeValue := strings.Trim(string(rawStepDetail["type"]), "\"")
	var stepDetail RunStepDetails
	if typeValue == "message_creation" {
		messageCreation := new(MessageCreationStepDetail)
		if err = unmarshalOptional(raw, "step_details", messageCreation); err != nil {
			return err
		}
		stepDetail = messageCreation
	} else if typeValue == "tool_calls" {
		toolCalls := new(ToolCallsStepDetail)
		if err = unmarshalOptional(raw, "step_details", toolCalls); err != nil {
			return err
		}
		stepDetail = toolCalls
	} else {
		return fmt.Errorf("cannot unmarshal json. unknown type %s", typeValue)
	}

	r.StepDetails = stepDetail
	return nil
}
