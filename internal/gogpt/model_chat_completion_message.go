package gogpt

import "encoding/json"

type ConversationMessage interface {
	GetRole() MessageRole
}

type BaseMessage struct {
	// The contents of the system message.
	Content string `json:"content"`
	// The role of the messages author, in this case system.
	Role MessageRole `json:"role"`
}

func (cm BaseMessage) GetRole() MessageRole {
	return cm.Role
}

type SystemMessage struct {
	BaseMessage
	// An optional name for the participant. Provides the model information to differentiate between participants of the same role.
	Name string `json:"name,omitempty"`
}

func NewSystemMessage(content string, name *string) *SystemMessage {
	return &SystemMessage{
		BaseMessage: BaseMessage{
			Content: content,
			Role:    MessageRoleSystem,
		},
		Name: stringValueOrEmpty(name),
	}
}

type UserMessage struct {
	Role    MessageRole        `json:"role"`
	Content UserMessageContent `json:"content"`
	// An optional name for the participant. Provides the model information to differentiate between participants of the same role.
	Name string `json:"name,omitempty"`
}

func (um UserMessage) GetRole() MessageRole {
	return um.Role
}

type UserMessageContent interface {
	Type() string
}

type TextUserMessageContent struct {
	Value string
}

func (c TextUserMessageContent) MarshalJSON() ([]byte, error) {
	return json.Marshal(c.Value)
}

func (c TextUserMessageContent) Type() string {
	return "text"
}

type ArrayUserMessageContent struct {
	Value []ArrayUserMessageContentPart
}

func (c ArrayUserMessageContent) MarshalJSON() ([]byte, error) {
	return json.Marshal(c.Value)
}

func (c ArrayUserMessageContent) Type() string {
	return "array"
}

type ArrayUserMessageContentPart interface {
	Type() string
}

type TextUserMessageContentPart struct {
	T    string `json:"type"`
	Text string `json:"text"`
}

func (cp TextUserMessageContentPart) Type() string {
	return cp.T
}

type ImageUserMessageContentPart struct {
	T        string   `json:"type"`
	ImageURL ImageURL `json:"image_url"`
}

func (cp ImageUserMessageContentPart) Type() string {
	return cp.T
}

type ImageURL struct {
	URL    string `json:"url"`
	Detail string `json:"detail,omitempty"`
}

type AssistantMessage struct {
	BaseMessage
	Name      string     `json:"name,omitempty"`
	ToolCalls []ToolCall `json:"tool_calls,omitempty"`
}

type ToolMessage struct {
	BaseMessage
	ToolCallID string `json:"tool_call_id"`
}

func stringValueOrEmpty(s *string) string {
	if s == nil {
		return ""
	}

	return *s
}
