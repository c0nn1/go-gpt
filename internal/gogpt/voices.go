package gogpt

type Voice string

const (
	VoiceAlloy   Voice = "alloy"
	VoiceEcho    Voice = "echo"
	VoiceFable   Voice = "fable"
	VoiceOnyx    Voice = "onyx"
	VoiceNova    Voice = "nova"
	VoiceShimmer Voice = "shimmer"
)

var Voices = []Voice{
	VoiceAlloy,
	VoiceEcho,
	VoiceFable,
	VoiceOnyx,
	VoiceNova,
	VoiceShimmer,
}

type SpeechResponseFormat string

const (
	SpeechFormatMP3  SpeechResponseFormat = "mp3"
	SpeechFormatOPUS SpeechResponseFormat = "opus"
	SpeechFormatAAC  SpeechResponseFormat = "aac"
	SpeechFormatFLAC SpeechResponseFormat = "flac"
)

var SpeechResponseFormats = []SpeechResponseFormat{
	SpeechFormatMP3,
	SpeechFormatOPUS,
	SpeechFormatAAC,
	SpeechFormatFLAC,
}

type TranscriptionResponseFormat string

const (
	TranscriptionFormatJSON        TranscriptionResponseFormat = "json"
	TranscriptionFormatText        TranscriptionResponseFormat = "text"
	TranscriptionFormatSRT         TranscriptionResponseFormat = "srt"
	TranscriptionFormatVerboseJSON TranscriptionResponseFormat = "verbose_json"
	TranscriptionFormatVTT         TranscriptionResponseFormat = "vtt"
)

var TranscriptionResponseFormats = []TranscriptionResponseFormat{
	TranscriptionFormatJSON,
	TranscriptionFormatText,
	TranscriptionFormatSRT,
	TranscriptionFormatVerboseJSON,
	TranscriptionFormatVTT,
}
