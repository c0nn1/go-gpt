package gogpt

// ChatCompletionResponse represents a chatCompletion completion response returned by model, based on the provided input.
type ChatCompletionResponse struct {
	// A unique identifier for the chatCompletion completion.
	ID string `json:"id"`
	// A list of chatCompletion completion choices. Can be more than one if n is greater than 1.
	Choices []Choice `json:"choices"`
	// The Unix timestamp (in seconds) of when the chatCompletion completion was created.
	Created int `json:"created"`
	// The model used for the chatCompletion completion.
	Model string `json:"model"`
	// This fingerprint represents the backend configuration that the model runs with.
	// Can be used in conjunction with the seed request parameter to understand when backend changes have been made that might impact determinism.
	SystemFingerprint string `json:"system_fingerprint"`
	// The object type, which is always chatCompletion.completion.
	Object string `json:"object"`
	// Usage statistics for the completion request.
	Usage Usage `json:"usage"`
}
