package gogpt

import "encoding/json"

func unmarshalOptional(values map[string]json.RawMessage, key string, target any) error {
	if v, ok := values[key]; ok {
		if err := json.Unmarshal(v, target); err != nil {
			return nil
		}
	}

	return nil
}
