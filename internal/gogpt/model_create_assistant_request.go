package gogpt

type CreateAssistantRequest struct {
	Model        Model     `json:"model"`
	Name         *string   `json:"name,omitempty"`
	Description  *string   `json:"description,omitempty"`
	Instructions *string   `json:"instructions,omitempty"`
	Tools        []RunTool `json:"tools,omitempty"`
	FileIDs      []string  `json:"file_ids,omitempty"`
	Metadata     Metadata  `json:"metadata"`
}

func NewCreateAssistantRequest(model Model, name *string, description *string, instructions *string, tools []RunTool, fileIDs []string, metadata Metadata) *CreateAssistantRequest {
	return &CreateAssistantRequest{
		Model:        model,
		Name:         name,
		Description:  description,
		Instructions: instructions,
		Tools:        tools,
		FileIDs:      fileIDs,
		Metadata:     metadata,
	}
}

type CreateAssistantRequestBuilder struct {
	model        Model
	name         *string
	description  *string
	instructions *string
	tools        []RunTool
	fileIDs      []string
	metadata     Metadata
}

func NewCreateAssistantRequestBuilder(m Model) *CreateAssistantRequestBuilder {
	return &CreateAssistantRequestBuilder{model: m}
}

func (b *CreateAssistantRequestBuilder) Build() *CreateAssistantRequest {
	return &CreateAssistantRequest{
		Model:        b.model,
		Name:         b.name,
		Description:  b.description,
		Instructions: b.instructions,
		Tools:        b.tools,
		FileIDs:      b.fileIDs,
		Metadata:     b.metadata,
	}
}

func (b *CreateAssistantRequestBuilder) WithName(n string) *CreateAssistantRequestBuilder {
	b.name = &n
	return b
}

func (b *CreateAssistantRequestBuilder) WithDescription(d string) *CreateAssistantRequestBuilder {
	b.description = &d
	return b
}

func (b *CreateAssistantRequestBuilder) WithInstructions(i string) *CreateAssistantRequestBuilder {
	b.instructions = &i
	return b
}

func (b *CreateAssistantRequestBuilder) WithTools(t []RunTool) *CreateAssistantRequestBuilder {
	b.tools = t
	return b
}

func (b *CreateAssistantRequestBuilder) WithFileIDs(ids []string) *CreateAssistantRequestBuilder {
	b.fileIDs = ids
	return b
}

func (b *CreateAssistantRequestBuilder) WithMetadata(m Metadata) *CreateAssistantRequestBuilder {
	b.metadata = m
	return b
}
