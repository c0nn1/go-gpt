package gogpt

type ThreadID string

type Thread struct {
	ID        ThreadID `json:"id"`
	Object    string   `json:"object"`
	CreatedAt int      `json:"created_at"`
	Metadata  Metadata `json:"metadata"`
}

type ThreadStartMessage struct {
	Role     MessageRole `json:"role"`
	Content  string      `json:"content"`
	FileIDs  []FileID    `json:"file_ids,omitempty"`
	Metadata Metadata    `json:"metadata,omitempty"`
}
