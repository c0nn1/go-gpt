package gogpt

type ModifyRunRequest struct {
	Metadata Metadata `json:"metadata"`
}

func NewModifyRunRequest(metadata Metadata) *ModifyRunRequest {
	return &ModifyRunRequest{Metadata: metadata}
}
