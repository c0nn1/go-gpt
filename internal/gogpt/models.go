package gogpt

type Model string

const (
    ModelWhisper1           Model = "whisper-1"
    ModelTTS1               Model = "tts-1"
    ModelTTS1_HD            Model = "tts-1-hd"
    ModelGPT4_VisionPreview Model = "gpt-4-vision-preview"
    ModelGPT4o              Model = "gpt-4o"
    ModelGPT4               Model = "gpt-4"
    ModelGPT4_Turbo         Model = "gpt-4-turbo"
    ModelGPT4_TurboPreview  Model = "gpt-4-turbo-preview"
    ModelGPT35_Turbo        Model = "gpt-3.5-turbo"
    ModelDalle3             Model = "dall-e-3"
    ModelDalle2             Model = "dall-e-2"
)

var ChatCompletionModels = []Model{
	ModelGPT4_VisionPreview,
    ModelGPT4o,
	ModelGPT4,
    ModelGPT4_Turbo,
    ModelGPT4_TurboPreview,
	ModelGPT35_Turbo,
}

var TextToSpeechModels = []Model{
	ModelTTS1,
	ModelTTS1_HD,
}
