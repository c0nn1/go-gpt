package gogpt

import (
	"context"
	"encoding/json"
	"io"
	"net/http"
	"net/url"
)

type AssistantsAPIService service

type CreateAssistantAPIRequest struct {
	ctx           context.Context
	createRequest *CreateAssistantRequest
	API           *AssistantsAPIService
}

func (e *AssistantsAPIService) Create(ctx context.Context) CreateAssistantAPIRequest {
	return CreateAssistantAPIRequest{
		ctx: ctx,
		API: e,
	}
}

func (r CreateAssistantAPIRequest) CreateRequest(req *CreateAssistantRequest) CreateAssistantAPIRequest {
	r.createRequest = req
	return r
}

func (r CreateAssistantAPIRequest) Execute() (*Assistant, *http.Response, error) {
	return r.API.CreateExecute(r)
}

func (e *AssistantsAPIService) CreateExecute(r CreateAssistantAPIRequest) (*Assistant, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "AssistantsAPIService.Create")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "assistants")
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeJSON().AddAssistantBeta()

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodPost, r.createRequest, headerParams, nil, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, resp, nil
	}

	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp, nil
	}

	if resp.StatusCode >= 300 {
		return nil, resp, NewOpenAIAPIError(respBody, resp.Status, nil)
	}

	returnValue := new(Assistant)
	if err = json.Unmarshal(respBody, returnValue); err != nil {
		return returnValue, resp, NewOpenAIAPIError(respBody, err.Error(), nil)
	}

	return returnValue, resp, nil
}

type GetAssistantAPIRequest struct {
	ctx context.Context
	id  AssistantID
	API *AssistantsAPIService
}

func (e *AssistantsAPIService) Get(ctx context.Context, id AssistantID) GetAssistantAPIRequest {
	return GetAssistantAPIRequest{
		ctx: ctx,
		id:  id,
		API: e,
	}
}

func (r GetAssistantAPIRequest) Execute() (*Assistant, *http.Response, error) {
	return r.API.GetExecute(r)
}

func (e *AssistantsAPIService) GetExecute(r GetAssistantAPIRequest) (*Assistant, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "AssistantsAPIService.Get")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "assistants", string(r.id))
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeJSON().AddAssistantBeta()

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodGet, nil, headerParams, nil, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, resp, nil
	}

	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp, nil
	}

	if resp.StatusCode >= 300 {
		return nil, resp, NewOpenAIAPIError(respBody, resp.Status, nil)
	}

	returnValue := new(Assistant)
	if err = json.Unmarshal(respBody, returnValue); err != nil {
		return returnValue, resp, NewOpenAIAPIError(respBody, err.Error(), nil)
	}

	return returnValue, resp, nil
}

type ModifyAssistantAPIRequest struct {
	ctx           context.Context
	id            AssistantID
	modifyRequest *ModifyAssistantRequest
	API           *AssistantsAPIService
}

func (e *AssistantsAPIService) Modify(ctx context.Context, id AssistantID) ModifyAssistantAPIRequest {
	return ModifyAssistantAPIRequest{
		ctx: ctx,
		id:  id,
		API: e,
	}
}

func (r ModifyAssistantAPIRequest) ModifyRequest(req *ModifyAssistantRequest) ModifyAssistantAPIRequest {
	r.modifyRequest = req
	return r
}

func (r ModifyAssistantAPIRequest) Execute() (*Assistant, *http.Response, error) {
	return r.API.ModifyExecute(r)
}

func (e *AssistantsAPIService) ModifyExecute(r ModifyAssistantAPIRequest) (*Assistant, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "AssistantsAPIService.Modify")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "assistants", string(r.id))
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeJSON().AddAssistantBeta()

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodPost, r.modifyRequest, headerParams, nil, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, resp, nil
	}

	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp, nil
	}

	if resp.StatusCode >= 300 {
		return nil, resp, NewOpenAIAPIError(respBody, resp.Status, nil)
	}

	returnValue := new(Assistant)
	if err = json.Unmarshal(respBody, returnValue); err != nil {
		return returnValue, resp, NewOpenAIAPIError(respBody, err.Error(), nil)
	}

	return returnValue, resp, nil
}

type DeleteAssistantAPIRequest struct {
	ctx context.Context
	id  AssistantID
	API *AssistantsAPIService
}

func (e *AssistantsAPIService) Delete(ctx context.Context, id AssistantID) DeleteAssistantAPIRequest {
	return DeleteAssistantAPIRequest{
		ctx: ctx,
		id:  id,
		API: e,
	}
}

func (r DeleteAssistantAPIRequest) Execute() (*DeleteResponse, *http.Response, error) {
	return r.API.DeleteExecute(r)
}

func (e *AssistantsAPIService) DeleteExecute(r DeleteAssistantAPIRequest) (*DeleteResponse, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "AssistantsAPIService.Delete")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "assistants", string(r.id))
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeJSON().AddAssistantBeta()

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodDelete, nil, headerParams, nil, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, resp, nil
	}

	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp, nil
	}

	if resp.StatusCode >= 300 {
		return nil, resp, NewOpenAIAPIError(respBody, resp.Status, nil)
	}

	returnValue := new(DeleteResponse)
	if err = json.Unmarshal(respBody, returnValue); err != nil {
		return returnValue, resp, NewOpenAIAPIError(respBody, err.Error(), nil)
	}

	return returnValue, resp, nil
}

type ListAssistantsAPIRequest struct {
	ctx  context.Context
	page *PageRequest
	API  *AssistantsAPIService
}

func (e *AssistantsAPIService) List(ctx context.Context) ListAssistantsAPIRequest {
	return ListAssistantsAPIRequest{
		ctx: ctx,
		API: e,
	}
}

func (r ListAssistantsAPIRequest) Page(p *PageRequest) ListAssistantsAPIRequest {
	r.page = p
	return r
}

func (r ListAssistantsAPIRequest) Execute() (*PagedAssistants, *http.Response, error) {
	return r.API.ListExecute(r)
}

func (e *AssistantsAPIService) ListExecute(r ListAssistantsAPIRequest) (*PagedAssistants, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "AssistantsAPIService.List")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "assistants")
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeJSON().AddAssistantBeta()

	queryParams := url.Values{}
	r.page.Query(queryParams)

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodGet, nil, headerParams, queryParams, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, resp, nil
	}

	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp, nil
	}

	if resp.StatusCode >= 300 {
		return nil, resp, NewOpenAIAPIError(respBody, resp.Status, nil)
	}

	returnValue := new(PagedAssistants)
	if err = json.Unmarshal(respBody, returnValue); err != nil {
		return returnValue, resp, NewOpenAIAPIError(respBody, err.Error(), nil)
	}

	return returnValue, resp, nil
}
