package gogpt

// ChatCompletionChunkResponse represents a streamed chunk of a chatCompletion completion response returned by model, based on the provided input.
type ChatCompletionChunkResponse struct {
	// A unique identifier for the chatCompletion completion. Each chunk has the same ID.
	ID string `json:"id"`
	// A list of chatCompletion completion choices. Can be more than one if n is greater than 1.
	Choices []ChatCompletionChunkChoice `json:"choices"`
	// The Unix timestamp (in seconds) of when the chatCompletion completion was created. Each chunk has the same timestamp.
	Created int `json:"created"`
	// The model to generate the completion.
	Model string `json:"model"`
	// This fingerprint represents the backend configuration that the model runs with. Can be used in conjunction with the seed request parameter to understand when backend changes have been made that might impact determinism.
	SystemFingerprint string `json:"system_fingerprint"`
	// The object type, which is always chatCompletion.completion.chunk.
	Object string `json:"object"`
}

type ChatCompletionChunkChoice struct {
	Delta            ChunkChoiceDelta          `json:"delta"`
	LogProbabilities ChunkChoiceLogProbability `json:"logprobs"`
	FinishReason     *string                   `json:"finish_reason"`
	Index            int                       `json:"index"`
}

type ChunkChoiceDelta struct {
	Content   NullableString    `json:"content"`
	ToolCalls []IndexedToolCall `json:"tool_calls"`
	Role      string            `json:"role"`
}

type IndexedToolCall struct {
	Index int `json:"index"`
	ToolCall
}

type ChunkChoiceLogProbability struct {
	Content []LogProbabilityContent `json:"content"`
}

type LogProbabilityContent struct {
	Token               string                  `json:"token"`
	LogProbability      float64                 `json:"logprob"`
	Bytes               []byte                  `json:"bytes"`
	TopLogProbabilities []LogProbabilityContent `json:"top_logprobs"`
}
