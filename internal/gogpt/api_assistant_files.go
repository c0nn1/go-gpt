package gogpt

import (
	"context"
	"encoding/json"
	"io"
	"net/http"
	"net/url"
)

type AssistantFilesAPIService service

type CreateAssistantFileAPIRequest struct {
	ctx           context.Context
	assistantID   AssistantID
	createRequest *CreateAssistantFileRequest
	API           *AssistantFilesAPIService
}

func (e *AssistantFilesAPIService) Create(ctx context.Context, assistantID AssistantID) CreateAssistantFileAPIRequest {
	return CreateAssistantFileAPIRequest{
		ctx:         ctx,
		assistantID: assistantID,
		API:         e,
	}
}

func (r CreateAssistantFileAPIRequest) CreateRequest(req *CreateAssistantFileRequest) CreateAssistantFileAPIRequest {
	r.createRequest = req
	return r
}

func (r CreateAssistantFileAPIRequest) Execute() (*AssistantFile, *http.Response, error) {
	return r.API.CreateExecute(r)
}

func (e *AssistantFilesAPIService) CreateExecute(r CreateAssistantFileAPIRequest) (*AssistantFile, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "AssistantFilesAPIService.Create")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "assistants", string(r.assistantID), "files")
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeJSON().AddAssistantBeta()

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodPost, r.createRequest, headerParams, nil, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, resp, nil
	}

	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp, nil
	}

	if resp.StatusCode >= 300 {
		return nil, resp, NewOpenAIAPIError(respBody, resp.Status, nil)
	}

	returnValue := new(AssistantFile)
	if err = json.Unmarshal(respBody, returnValue); err != nil {
		return returnValue, resp, NewOpenAIAPIError(respBody, err.Error(), nil)
	}

	return returnValue, resp, nil
}

type GetAssistantFileAPIRequest struct {
	ctx             context.Context
	assistantID     AssistantID
	assistantFileID AssistantFileID
	API             *AssistantFilesAPIService
}

func (e *AssistantFilesAPIService) Get(ctx context.Context, assistantID AssistantID, assistantFileID AssistantFileID) GetAssistantFileAPIRequest {
	return GetAssistantFileAPIRequest{
		ctx:             ctx,
		assistantID:     assistantID,
		assistantFileID: assistantFileID,
		API:             e,
	}
}

func (r GetAssistantFileAPIRequest) Execute() (*AssistantFile, *http.Response, error) {
	return r.API.GetExecute(r)
}

func (e *AssistantFilesAPIService) GetExecute(r GetAssistantFileAPIRequest) (*AssistantFile, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "AssistantFilesAPIService.Get")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "assistants", string(r.assistantID), "files", string(r.assistantFileID))
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeJSON().AddAssistantBeta()

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodGet, nil, headerParams, nil, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, resp, nil
	}

	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp, nil
	}

	if resp.StatusCode >= 300 {
		return nil, resp, NewOpenAIAPIError(respBody, resp.Status, nil)
	}

	returnValue := new(AssistantFile)
	if err = json.Unmarshal(respBody, returnValue); err != nil {
		return returnValue, resp, NewOpenAIAPIError(respBody, err.Error(), nil)
	}

	return returnValue, resp, nil
}

type DeleteAssistantFileAPIRequest struct {
	ctx         context.Context
	assistantID AssistantID
	fileID      AssistantFileID
	API         *AssistantFilesAPIService
}

func (e *AssistantFilesAPIService) Delete(ctx context.Context, assistantID AssistantID, fileID AssistantFileID) DeleteAssistantFileAPIRequest {
	return DeleteAssistantFileAPIRequest{
		ctx:         ctx,
		assistantID: assistantID,
		fileID:      fileID,
		API:         e,
	}
}

func (r DeleteAssistantFileAPIRequest) Execute() (*DeleteResponse, *http.Response, error) {
	return r.API.DeleteExecute(r)
}

func (e *AssistantFilesAPIService) DeleteExecute(r DeleteAssistantFileAPIRequest) (*DeleteResponse, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "AssistantFilesAPIService.Delete")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "assistants", string(r.assistantID), "files", string(r.fileID))
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeJSON().AddAssistantBeta()

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodDelete, nil, headerParams, nil, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, resp, nil
	}

	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp, nil
	}

	if resp.StatusCode >= 300 {
		return nil, resp, NewOpenAIAPIError(respBody, resp.Status, nil)
	}

	returnValue := new(DeleteResponse)
	if err = json.Unmarshal(respBody, returnValue); err != nil {
		return returnValue, resp, NewOpenAIAPIError(respBody, err.Error(), nil)
	}

	return returnValue, resp, nil
}

type ListAssistantFilesAPIRequest struct {
	ctx         context.Context
	assistantID AssistantID
	page        *PageRequest
	API         *AssistantFilesAPIService
}

func (e *AssistantFilesAPIService) List(ctx context.Context, assistantID AssistantID) ListAssistantFilesAPIRequest {
	return ListAssistantFilesAPIRequest{
		ctx:         ctx,
		assistantID: assistantID,
		API:         e,
	}
}

func (r ListAssistantFilesAPIRequest) Page(p *PageRequest) ListAssistantFilesAPIRequest {
	r.page = p
	return r
}

func (r ListAssistantFilesAPIRequest) Execute() (*PagedAssistantFiles, *http.Response, error) {
	return r.API.ListExecute(r)
}

func (e *AssistantFilesAPIService) ListExecute(r ListAssistantFilesAPIRequest) (*PagedAssistantFiles, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "AssistantFilesAPIService.List")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "assistants", string(r.assistantID), "files")
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeJSON().AddAssistantBeta()

	queryParams := url.Values{}
	r.page.Query(queryParams)

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodGet, nil, headerParams, queryParams, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, resp, nil
	}

	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp, nil
	}

	if resp.StatusCode >= 300 {
		return nil, resp, NewOpenAIAPIError(respBody, resp.Status, nil)
	}

	returnValue := new(PagedAssistantFiles)
	if err = json.Unmarshal(respBody, returnValue); err != nil {
		return returnValue, resp, NewOpenAIAPIError(respBody, err.Error(), nil)
	}

	return returnValue, resp, nil
}
