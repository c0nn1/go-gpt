package gogpt

import (
	"context"
	"encoding/json"
	"io"
	"net/http"
	"net/url"
	"strconv"
)

type ImagesAPIService service

type ImageResponseFormat string

const (
	ImageFormatURL     ImageResponseFormat = "url"
	ImageFormatB64JSON ImageResponseFormat = "b64_json"
)

type ImageSize string

const (
	ImageSize256       ImageSize = "256x256"
	ImageSize512       ImageSize = "512x512"
	ImageSize1024      ImageSize = "1024x1024"
	ImageSize1792x1024 ImageSize = "1792x1024"
	ImageSize1024x1792 ImageSize = "1024x1792"
)

type ImageStyle string

const (
	ImageStyleVivid   ImageStyle = "vivid"
	ImageStyleNatural ImageStyle = "natural"
)

type ImageQuality string

const (
	ImageQualityHD       ImageQuality = "hd"
	ImageQualityStandard ImageQuality = "standard"
)

type CreateImageAPIRequest struct {
	ctx           context.Context
	createRequest *CreateImageRequest
	API           *ImagesAPIService
}

type CreateImageEditAPIRequest struct {
	ctx               context.Context
	createEditRequest *CreateImageEditRequest
	API               *ImagesAPIService
}

func (e *ImagesAPIService) Create(ctx context.Context) CreateImageAPIRequest {
	return CreateImageAPIRequest{
		ctx: ctx,
		API: e,
	}
}

func (r CreateImageAPIRequest) CreateRequest(req *CreateImageRequest) CreateImageAPIRequest {
	r.createRequest = req
	return r
}

func (r CreateImageAPIRequest) Execute() (*CreateImageResponse, *http.Response, error) {
	return r.API.CreateExecute(r)
}

func (e *ImagesAPIService) CreateExecute(r CreateImageAPIRequest) (*CreateImageResponse, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "ImagesAPIService.Create")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "images", "generations")
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeJSON()

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodPost, r.createRequest, headerParams, nil, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, resp, err
	}

	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp, err
	}

	if resp.StatusCode >= 300 {
		return nil, resp, NewOpenAIAPIError(respBody, resp.Status, nil)
	}

	returnValue := new(CreateImageResponse)
	if err = json.Unmarshal(respBody, returnValue); err != nil {
		return returnValue, resp, NewOpenAIAPIError(respBody, err.Error(), nil)
	}

	return returnValue, resp, nil
}

func (e *ImagesAPIService) CreateEdit(ctx context.Context) CreateImageEditAPIRequest {
	return CreateImageEditAPIRequest{
		ctx: ctx,
		API: e,
	}
}

func (r CreateImageEditAPIRequest) CreateEditRequest(req *CreateImageEditRequest) CreateImageEditAPIRequest {
	r.createEditRequest = req
	return r
}

func (r CreateImageEditAPIRequest) Execute() (*CreateImageEditResponse, *http.Response, error) {
	return r.API.CreateEditExecute(r)
}

func (e *ImagesAPIService) CreateEditExecute(r CreateImageEditAPIRequest) (*CreateImageEditResponse, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "ImagesAPIService.CreateEdit")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "images", "edits")
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeFormData()

	formParams := url.Values{}
	formParams.Set("@image", r.createEditRequest.FilePath)
	formParams.Set("prompt", r.createEditRequest.Prompt)
	if len(r.createEditRequest.MaskFilePath) > 0 {
		formParams.Set("@mask", r.createEditRequest.MaskFilePath)
	}

	if len(r.createEditRequest.Model) > 0 {
		formParams.Set("model", string(r.createEditRequest.Model))
	}

	if len(r.createEditRequest.ResponseFormat) > 0 {
		formParams.Set("response_format", string(r.createEditRequest.ResponseFormat))
	}

	if len(r.createEditRequest.Size) > 0 {
		formParams.Set("size", r.createEditRequest.Size)
	}

	if len(r.createEditRequest.User) > 0 {
		formParams.Set("user", r.createEditRequest.User)
	}

	if r.createEditRequest.N != nil {
		formParams.Set("n", strconv.Itoa(*r.createEditRequest.N))
	}

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodPost, nil, headerParams, nil, formParams, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, nil, err
	}

	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp, err
	}

	if resp.StatusCode >= 300 {
		return nil, resp, NewOpenAIAPIError(respBody, resp.Status, nil)
	}

	returnValue := new(CreateImageEditResponse)
	if err = json.Unmarshal(respBody, returnValue); err != nil {
		return nil, resp, NewOpenAIAPIError(respBody, err.Error(), nil)
	}

	return returnValue, resp, nil
}
