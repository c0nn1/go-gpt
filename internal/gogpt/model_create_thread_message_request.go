package gogpt

type CreateThreadMessageRequest struct {
	Role     MessageRole `json:"role"`
	Content  string      `json:"content"`
	FileIDs  []FileID    `json:"file_ids,omitempty"`
	Metadata Metadata    `json:"metadata,omitempty"`
}

func NewCreateThreadMessageRequest(role MessageRole, content string, fileIDs []FileID, metadata Metadata) *CreateThreadMessageRequest {
	return &CreateThreadMessageRequest{Role: role, Content: content, FileIDs: fileIDs, Metadata: metadata}
}
