package gogpt

import (
	"encoding/json"
	"fmt"
	"strings"
)

type ThreadMessageID string

type ThreadMessage struct {
	ID          ThreadMessageID       `json:"id"`
	Object      string                `json:"object"`
	CreatedAt   int                   `json:"created_at"`
	ThreadID    ThreadID              `json:"thread_id"`
	Role        MessageRole           `json:"role"`
	Content     ThreadMessageContents `json:"content"`
	AssistantID *string               `json:"assistant_id"`
	RunID       *string               `json:"run_id"`
	FileIDs     []string              `json:"file_ids"`
	Metadata    Metadata              `json:"metadata"`
}

type ThreadMessageContents []ThreadMessageContent

func (tc *ThreadMessageContents) UnmarshalJSON(data []byte) error {
	raw := make([]json.RawMessage, 0, 5)
	if err := json.Unmarshal(data, &raw); err != nil {
		return err
	}

	contents := make(ThreadMessageContents, len(raw))
	for i, v := range raw {
		fields := make(map[string]json.RawMessage)
		if err := json.Unmarshal(v, &fields); err != nil {
			return err
		}

		var messageContent ThreadMessageContent
		typeValue := strings.Trim(string(fields["type"]), "\"")
		if typeValue == "image_file" {
			messageContent = new(ImageMessageContent)
		} else if typeValue == "text" {
			messageContent = new(TextMessageContent)
		} else {
			return fmt.Errorf("cannot unmarshal json. unknown type %s", typeValue)
		}

		if err := json.Unmarshal(v, &messageContent); err != nil {
			return err
		}

		contents[i] = messageContent
	}

	*tc = contents
	return nil
}

type ThreadMessageContent interface {
	Type() string
}

type MessageType struct {
	T string `json:"type"`
}

func (mt MessageType) Type() string {
	return mt.T
}

type ImageMessageContent struct {
	MessageType
	ImageFile ImageFile `json:"image_file"`
}

type ImageFile struct {
	FileID string `json:"file_id"`
}

type TextMessageContent struct {
	MessageType
	Text TextContent `json:"text"`
}

type TextContent struct {
	Value       string                 `json:"value"`
	Annotations TextMessageAnnotations `json:"annotations"`
}

type TextMessageAnnotations []TextMessageAnnotation

func (ta *TextMessageAnnotations) UnmarshalJSON(data []byte) error {
	var err error

	raw := new([]json.RawMessage)
	if err = json.Unmarshal(data, raw); err != nil {
		return err
	}

	annotations := make(TextMessageAnnotations, len(*raw))
	for i, v := range *raw {
		var fields map[string]json.RawMessage
		if err = json.Unmarshal(v, &fields); err != nil {
			return err
		}

		typeValue := strings.Trim(string(fields["type"]), "\"")
		var annotation TextMessageAnnotation
		if typeValue == "file_citation" {
			annotation = new(FileCitationAnnotation)
		} else if typeValue == "file_path" {
			annotation = new(FilePathAnnotation)
		} else {
			return fmt.Errorf("cannot unmarshal json. unknown type %s", typeValue)
		}

		if err = json.Unmarshal(v, &annotation); err != nil {
			return err
		}

		annotations[i] = annotation
	}

	*ta = annotations
	return nil
}

type TextMessageAnnotation interface {
	Type() string
	Text() string
}

type FileCitationAnnotation struct {
	Ty           string       `json:"type"`
	Te           string       `json:"text"`
	FileCitation FileCitation `json:"file_citation"`
	StartIndex   int          `json:"start_index"`
	EndIndex     int          `json:"end_index"`
}

func (a FileCitationAnnotation) Type() string {
	return a.Ty
}

func (a FileCitationAnnotation) Text() string {
	return a.Te
}

type FileCitation struct {
	FileID string `json:"file_id"`
	Quote  string `json:"quote"`
}

type FilePathAnnotation struct {
	Ty         string   `json:"type"`
	Te         string   `json:"text"`
	FilePath   FilePath `json:"file_path"`
	StartIndex int      `json:"start_index"`
	EndIndex   int      `json:"end_index"`
}

func (a FilePathAnnotation) Type() string {
	return a.Ty
}

func (a FilePathAnnotation) Text() string {
	return a.Te
}

type FilePath struct {
	FileID string `json:"file_id"`
}
