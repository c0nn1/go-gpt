package gogpt

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"mime/multipart"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

var (
	jsonCheck       = regexp.MustCompile(`(?i:(?:application|text)/(?:vnd\.[^;]+\+)?json)`)
	queryParamSplit = regexp.MustCompile(`(^|&)([^&]+)`)
	queryDescape    = strings.NewReplacer("%5B", "[", "%5D", "]")
)

type APIClient struct {
	cfg    *Configuration
	common service

	AudioAPI              *AudioAPIService
	AssistantFilesAPI     *AssistantFilesAPIService
	AssistantsAPI         *AssistantsAPIService
	ChatCompletionAPI     *ChatCompletionAPIService
	FilesAPI              *FilesAPIService
	ImagesAPI             *ImagesAPIService
	RunStepsAPI           *RunStepsAPIService
	RunsAPI               *RunsAPIService
	ThreadMessageFilesAPI *ThreadMessageFilesAPIService
	ThreadMessagesAPI     *ThreadMessagesAPIService
	ThreadsAPI            *ThreadsAPIService
}

type service struct {
	client *APIClient
}

func NewAPIClient(cfg *Configuration) *APIClient {
	if cfg.HTTPClient == nil {
		cfg.HTTPClient = http.DefaultClient
	}

	c := &APIClient{}
	c.cfg = cfg
	c.common.client = c

	c.AudioAPI = (*AudioAPIService)(&c.common)
	c.AssistantFilesAPI = (*AssistantFilesAPIService)(&c.common)
	c.AssistantsAPI = (*AssistantsAPIService)(&c.common)
	c.ChatCompletionAPI = (*ChatCompletionAPIService)(&c.common)
	c.FilesAPI = (*FilesAPIService)(&c.common)
	c.ImagesAPI = (*ImagesAPIService)(&c.common)
	c.RunStepsAPI = (*RunStepsAPIService)(&c.common)
	c.RunsAPI = (*RunsAPIService)(&c.common)
	c.ThreadMessageFilesAPI = (*ThreadMessageFilesAPIService)(&c.common)
	c.ThreadMessagesAPI = (*ThreadMessagesAPIService)(&c.common)
	c.ThreadsAPI = (*ThreadsAPIService)(&c.common)

	return c
}

type formFile struct {
	fileBytes    []byte
	fileName     string
	formFileName string
}

func addFile(w *multipart.Writer, fieldName, path string) error {
	var err error
	var file io.ReadCloser
	var fileName string

	if strings.HasPrefix(path, "https://") {
		file, fileName, err = getRemoteFile(path)
	} else {
		file, fileName, err = getLocalFile(path)
	}

	defer file.Close()

	part, err := w.CreateFormFile(fieldName, fileName)
	if err != nil {
		return err
	}

	_, err = io.Copy(part, file)

	return err
}

func getLocalFile(path string) (file io.ReadCloser, fileName string, err error) {
	file, err = os.Open(filepath.Clean(path))
	if err != nil {
		return nil, fileName, err
	}

	fileName = filepath.Base(path)
	return file, fileName, nil
}

func getRemoteFile(path string) (file io.ReadCloser, fileName string, err error) {
	u, err := url.Parse(path)
	if err != nil {
		return nil, fileName, fmt.Errorf("failed to parse path into url: %v", err)
	}

	resp, err := http.Get(path)
	if err != nil {
		return nil, fileName, err
	}

	if resp.StatusCode != http.StatusOK {
		resp.Body.Close()
		return nil, fileName, fmt.Errorf("failed to fetch file: HTTP status code: %d", resp.StatusCode)
	}

	fileName = filepath.Base(u.Path)
	file = resp.Body
	return file, fileName, nil
}

func setBody(body any, contentType string) (bodyBuf *bytes.Buffer, err error) {
	if bodyBuf == nil {
		bodyBuf = &bytes.Buffer{}
	}

	if reader, ok := body.(io.Reader); ok {
		_, err = bodyBuf.ReadFrom(reader)
	} else if fp, ok := body.(*os.File); ok {
		_, err = bodyBuf.ReadFrom(fp)
	} else if b, ok := body.([]byte); ok {
		_, err = bodyBuf.Write(b)
	} else if s, ok := body.(string); ok {
		_, err = bodyBuf.WriteString(s)
	} else if s, ok := body.(*string); ok {
		_, err = bodyBuf.WriteString(*s)
	} else if jsonCheck.MatchString(contentType) {
		err = json.NewEncoder(bodyBuf).Encode(body)
	}

	if err != nil {
		return nil, err
	}

	if bodyBuf.Len() == 0 {
		err = fmt.Errorf("invalid body type %s\n", contentType)
		return nil, err
	}

	return bodyBuf, nil
}

func (c *APIClient) prepareRequest(
	ctx context.Context,
	path string, method string,
	postBody any,
	headerParams map[string]string,
	queryParams url.Values,
	formParams url.Values,
	formFiles []formFile) (req *http.Request, err error) {

	var body *bytes.Buffer

	contentType := headerParams["Content-Type"]
	if postBody != nil {
		if len(contentType) == 0 {
			return req, fmt.Errorf("expected content-type but got none")
		}

		body, err = setBody(postBody, contentType)
		if err != nil {
			return nil, err
		}
	}

	if strings.HasPrefix(contentType, "multipart/form-data") && (len(formParams) > 0 || (len(formFiles) > 0)) {
		if body != nil {
			return nil, errors.New("cannot specify postBody and multipart form at the same time")
		}

		body = &bytes.Buffer{}
		w := multipart.NewWriter(body)

		for k, v := range formParams {
			for _, iv := range v {
				if strings.HasPrefix(k, "@") {
					// file
					err = addFile(w, k[1:], iv)
					if err != nil {
						return nil, err
					}
				} else {
					// form value
					_ = w.WriteField(k, iv)
				}
			}
		}

		for _, formFile := range formFiles {
			if len(formFile.fileBytes) > 0 && formFile.fileName != "" {
				w.Boundary()
				part, err := w.CreateFormFile(formFile.formFileName, filepath.Base(formFile.fileName))
				if err != nil {
					return nil, err
				}
				_, err = part.Write(formFile.fileBytes)
				if err != nil {
					return nil, err
				}
			}
		}

		headerParams["Content-Type"] = w.FormDataContentType()
		headerParams["Content-Length"] = fmt.Sprintf("%d", body.Len())
		_ = w.Close()
	}

	url, err := url.Parse(path)
	if err != nil {
		return nil, err
	}

	if c.cfg.Host != "" {
		url.Host = c.cfg.Host
	}

	if c.cfg.Scheme != "" {
		url.Scheme = c.cfg.Scheme
	}

	query := url.Query()
	for k, v := range queryParams {
		for _, iv := range v {
			query.Add(k, iv)
		}
	}

	url.RawQuery = queryParamSplit.ReplaceAllStringFunc(query.Encode(), func(s string) string {
		pieces := strings.Split(s, "=")
		pieces[0] = queryDescape.Replace(pieces[0])
		return strings.Join(pieces, "=")
	})

	if body != nil {
		req, err = http.NewRequest(method, url.String(), body)
	} else {
		req, err = http.NewRequest(method, url.String(), nil)
	}

	if err != nil {
		return nil, err
	}

	if len(headerParams) > 0 {
		headers := http.Header{}
		for h, v := range headerParams {
			headers[h] = []string{v}
		}
		req.Header = headers
	}

	req.Header.Add("User-Agent", c.cfg.UserAgent)

	if ctx != nil {
		req = req.WithContext(ctx)

		if auth, ok := ctx.Value(ContextAccessToken).(string); ok {
			req.Header.Add("Authorization", "Bearer "+auth)
		}
	}

	for header, value := range c.cfg.DefaultHeader {
		req.Header.Add(header, value)
	}

	return req, nil
}

func (c *APIClient) callAPI(request *http.Request) (*http.Response, error) {
	if c.cfg.Debug {
		dump, err := httputil.DumpRequestOut(request, true)
		if err != nil {
			return nil, err
		}

		log.Printf("\n%s\n", string(dump))
	}

	resp, err := c.cfg.HTTPClient.Do(request)
	if err != nil {
		return resp, err
	}

	if c.cfg.Debug {
		dump, err := httputil.DumpResponse(resp, true)
		if err != nil {
			return resp, nil
		}
		log.Printf("\n%s\n", string(dump))
	}

	return resp, nil
}
