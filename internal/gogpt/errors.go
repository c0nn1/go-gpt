package gogpt

type OpenAIAPIError struct {
	body  []byte
	error string
	model any
}

func NewOpenAIAPIError(body []byte, error string, model any) *OpenAIAPIError {
	return &OpenAIAPIError{
		body:  body,
		error: error,
		model: model,
	}
}

func (e OpenAIAPIError) Body() []byte {
	return e.body
}

func (e OpenAIAPIError) Error() string {
	return e.error
}

func (e OpenAIAPIError) Model() any {
	return e.model
}
