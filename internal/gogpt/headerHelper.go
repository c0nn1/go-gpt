package gogpt

type openaiHeader map[string]string

func (h openaiHeader) AddContentTypeJSON() openaiHeader {
	h["Content-Type"] = "application/json"
	return h
}

func (h openaiHeader) AddContentTypeFormData() openaiHeader {
	h["Content-Type"] = "multipart/form-data"
	return h
}

//func (h openaiHeader) AddAuth(token string) openaiHeader {
//	h["Authorization"] = fmt.Sprintf("Bearer %s", token)
//	return h
//}

func (h openaiHeader) AddAssistantBeta() openaiHeader {
	h["OpenAI-Beta"] = "assistants=v1"
	return h
}
