package gogpt

import (
	"context"
	"encoding/json"
	"io"
	"net/http"
	"net/url"
)

type FilesAPIService service

type ListFilesAPIRequest struct {
	ctx  context.Context
	page *PageRequest
	API  *FilesAPIService
}

func (e *FilesAPIService) List(ctx context.Context) ListFilesAPIRequest {
	return ListFilesAPIRequest{
		ctx: ctx,
		API: e,
	}
}

func (r ListFilesAPIRequest) Page(p *PageRequest) ListFilesAPIRequest {
	r.page = p
	return r
}

func (r ListFilesAPIRequest) Execute() (*PagedFiles, *http.Response, error) {
	return r.API.ListExecute(r)
}

func (e *FilesAPIService) ListExecute(r ListFilesAPIRequest) (*PagedFiles, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "FilesAPIService.List")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "files")
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeJSON()

	queryParams := url.Values{}
	r.page.Query(queryParams)

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodGet, nil, headerParams, queryParams, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, resp, nil
	}

	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp, nil
	}

	if resp.StatusCode >= 300 {
		return nil, resp, NewOpenAIAPIError(respBody, resp.Status, nil)
	}

	returnValue := new(PagedFiles)
	if err = json.Unmarshal(respBody, returnValue); err != nil {
		return returnValue, resp, NewOpenAIAPIError(respBody, err.Error(), nil)
	}

	return returnValue, resp, nil
}

type CreateFileAPIRequest struct {
	ctx           context.Context
	createRequest *CreateFileRequest
	API           *FilesAPIService
}

func (e *FilesAPIService) Create(ctx context.Context) CreateFileAPIRequest {
	return CreateFileAPIRequest{
		ctx: ctx,
		API: e,
	}
}

func (r CreateFileAPIRequest) CreateRequest(req *CreateFileRequest) CreateFileAPIRequest {
	r.createRequest = req
	return r
}

func (r CreateFileAPIRequest) Execute() (*File, *http.Response, error) {
	return r.API.CreateExecute(r)
}

func (e *FilesAPIService) CreateExecute(r CreateFileAPIRequest) (*File, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "FilesAPIService.Create")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "files")
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeFormData()

	formParams := url.Values{}
	formParams.Set("purpose", string(r.createRequest.Purpose))
	formParams.Add("@file", r.createRequest.FilePath)

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodPost, nil, headerParams, nil, formParams, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, resp, nil
	}

	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp, nil
	}

	if resp.StatusCode >= 300 {
		return nil, resp, NewOpenAIAPIError(respBody, resp.Status, nil)
	}

	returnValue := new(File)
	if err = json.Unmarshal(respBody, returnValue); err != nil {
		return returnValue, resp, NewOpenAIAPIError(respBody, err.Error(), nil)
	}

	return returnValue, resp, nil
}

type DeleteFileAPIRequest struct {
	ctx context.Context
	id  FileID
	API *FilesAPIService
}

func (e *FilesAPIService) Delete(ctx context.Context, id FileID) DeleteFileAPIRequest {
	return DeleteFileAPIRequest{
		ctx: ctx,
		id:  id,
		API: e,
	}
}

func (r DeleteFileAPIRequest) Execute() (*DeleteResponse, *http.Response, error) {
	return r.API.DeleteExecute(r)
}

func (e *FilesAPIService) DeleteExecute(r DeleteFileAPIRequest) (*DeleteResponse, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "FilesAPIService.Delete")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "files", string(r.id))
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeJSON()

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodDelete, nil, headerParams, nil, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, resp, nil
	}

	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp, nil
	}

	if resp.StatusCode >= 300 {
		return nil, resp, NewOpenAIAPIError(respBody, resp.Status, nil)
	}

	returnValue := new(DeleteResponse)
	if err = json.Unmarshal(respBody, returnValue); err != nil {
		return returnValue, resp, NewOpenAIAPIError(respBody, err.Error(), nil)
	}

	return returnValue, resp, nil
}

type GetFileAPIRequest struct {
	ctx context.Context
	id  FileID
	API *FilesAPIService
}

func (e *FilesAPIService) Get(ctx context.Context, id FileID) GetFileAPIRequest {
	return GetFileAPIRequest{
		ctx: ctx,
		id:  id,
		API: e,
	}
}

func (r GetFileAPIRequest) Execute() (*File, *http.Response, error) {
	return r.API.GetExecute(r)
}

func (e *FilesAPIService) GetExecute(r GetFileAPIRequest) (*File, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "FilesAPIService.Get")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "files", string(r.id))
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeJSON()

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodGet, nil, headerParams, nil, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, resp, nil
	}

	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp, nil
	}

	if resp.StatusCode >= 300 {
		return nil, resp, NewOpenAIAPIError(respBody, resp.Status, nil)
	}

	returnValue := new(File)
	if err = json.Unmarshal(respBody, returnValue); err != nil {
		return returnValue, resp, NewOpenAIAPIError(respBody, err.Error(), nil)
	}

	return returnValue, resp, nil
}

type GetFileContentAPIRequest struct {
	ctx context.Context
	id  FileID
	API *FilesAPIService
}

func (e *FilesAPIService) GetContent(ctx context.Context, id FileID) GetFileContentAPIRequest {
	return GetFileContentAPIRequest{
		ctx: ctx,
		id:  id,
		API: e,
	}
}

func (r GetFileContentAPIRequest) Execute() ([]byte, *http.Response, error) {
	return r.API.GetContentExecute(r)
}

func (e *FilesAPIService) GetContentExecute(r GetFileContentAPIRequest) ([]byte, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "FilesAPIService.GetContent")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "files", string(r.id), "content")
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeJSON()

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodGet, nil, headerParams, nil, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, resp, nil
	}

	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp, nil
	}

	if resp.StatusCode >= 300 {
		return nil, resp, NewOpenAIAPIError(respBody, resp.Status, nil)
	}

	return respBody, resp, nil
}
