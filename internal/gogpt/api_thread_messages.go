package gogpt

import (
	"context"
	"encoding/json"
	"io"
	"net/http"
	"net/url"
)

type ThreadMessagesAPIService service

type CreateThreadMessageAPIRequest struct {
	ctx           context.Context
	threadID      ThreadID
	createRequest *CreateThreadMessageRequest
	API           *ThreadMessagesAPIService
}

func (e *ThreadMessagesAPIService) Create(ctx context.Context, threadID ThreadID) CreateThreadMessageAPIRequest {
	return CreateThreadMessageAPIRequest{
		ctx:      ctx,
		threadID: threadID,
		API:      e,
	}
}

func (r CreateThreadMessageAPIRequest) CreateRequest(req *CreateThreadMessageRequest) CreateThreadMessageAPIRequest {
	r.createRequest = req
	return r
}

func (r CreateThreadMessageAPIRequest) Execute() (*ThreadMessage, *http.Response, error) {
	return r.API.CreateExecute(r)
}

func (e *ThreadMessagesAPIService) CreateExecute(r CreateThreadMessageAPIRequest) (*ThreadMessage, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "ThreadMessagesAPIService.Create")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "threads", string(r.threadID), "messages")
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeJSON().AddAssistantBeta()

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodPost, r.createRequest, headerParams, nil, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, resp, nil
	}

	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp, nil
	}

	if resp.StatusCode >= 300 {
		return nil, resp, NewOpenAIAPIError(respBody, resp.Status, nil)
	}

	returnValue := new(ThreadMessage)
	if err = json.Unmarshal(respBody, returnValue); err != nil {
		return returnValue, resp, NewOpenAIAPIError(respBody, err.Error(), nil)
	}

	return returnValue, resp, nil
}

type GetThreadMessageAPIRequest struct {
	ctx       context.Context
	threadID  ThreadID
	messageID ThreadMessageID
	API       *ThreadMessagesAPIService
}

func (e *ThreadMessagesAPIService) Get(ctx context.Context, threadID ThreadID, messageID ThreadMessageID) GetThreadMessageAPIRequest {
	return GetThreadMessageAPIRequest{
		ctx:       ctx,
		threadID:  threadID,
		messageID: messageID,
		API:       e,
	}
}

func (r GetThreadMessageAPIRequest) Execute() (*ThreadMessage, *http.Response, error) {
	return r.API.GetExecute(r)
}

func (e *ThreadMessagesAPIService) GetExecute(r GetThreadMessageAPIRequest) (*ThreadMessage, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "ThreadMessagesAPIService.Get")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "threads", string(r.threadID), "messages", string(r.messageID))
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeJSON().AddAssistantBeta()

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodGet, nil, headerParams, nil, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, resp, nil
	}

	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp, nil
	}

	if resp.StatusCode >= 300 {
		return nil, resp, NewOpenAIAPIError(respBody, resp.Status, nil)
	}

	returnValue := new(ThreadMessage)
	if err = json.Unmarshal(respBody, returnValue); err != nil {
		return returnValue, resp, NewOpenAIAPIError(respBody, err.Error(), nil)
	}

	return returnValue, resp, nil
}

type ModifyThreadMessageAPIRequest struct {
	ctx       context.Context
	threadID  ThreadID
	messageID ThreadMessageID
	metadata  Metadata
	API       *ThreadMessagesAPIService
}

func (e *ThreadMessagesAPIService) Modify(ctx context.Context, threadID ThreadID, messageID ThreadMessageID) ModifyThreadMessageAPIRequest {
	return ModifyThreadMessageAPIRequest{
		ctx:       ctx,
		threadID:  threadID,
		messageID: messageID,
		API:       e,
	}
}

func (r ModifyThreadMessageAPIRequest) Metadata(m Metadata) ModifyThreadMessageAPIRequest {
	r.metadata = m
	return r
}

func (r ModifyThreadMessageAPIRequest) Execute() (*ThreadMessage, *http.Response, error) {
	return r.API.ModifyExecute(r)
}

func (e *ThreadMessagesAPIService) ModifyExecute(r ModifyThreadMessageAPIRequest) (*ThreadMessage, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "ThreadMessagesAPIService.Modify")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "threads", string(r.threadID), "messages", string(r.messageID))
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeJSON().AddAssistantBeta()

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodPost, r.metadata, headerParams, nil, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, resp, nil
	}

	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp, nil
	}

	if resp.StatusCode >= 300 {
		return nil, resp, NewOpenAIAPIError(respBody, resp.Status, nil)
	}

	returnValue := new(ThreadMessage)
	if err = json.Unmarshal(respBody, returnValue); err != nil {
		return returnValue, resp, NewOpenAIAPIError(respBody, err.Error(), nil)
	}

	return returnValue, resp, nil
}

type ListThreadMessagesAPIRequest struct {
	ctx      context.Context
	threadID ThreadID
	page     *PageRequest
	API      *ThreadMessagesAPIService
}

func (e *ThreadMessagesAPIService) List(ctx context.Context, threadID ThreadID) ListThreadMessagesAPIRequest {
	return ListThreadMessagesAPIRequest{
		ctx:      ctx,
		threadID: threadID,
		API:      e,
	}
}

func (r ListThreadMessagesAPIRequest) Page(p *PageRequest) ListThreadMessagesAPIRequest {
	r.page = p
	return r
}

func (r ListThreadMessagesAPIRequest) Execute() (*PagedThreadMessages, *http.Response, error) {
	return r.API.ListExecute(r)
}

func (e *ThreadMessagesAPIService) ListExecute(r ListThreadMessagesAPIRequest) (*PagedThreadMessages, *http.Response, error) {
	basePath, err := e.client.cfg.ServerURLWithContext(r.ctx, "ThreadMessagesAPIService.List")
	if err != nil {
		return nil, nil, err
	}

	path, err := url.JoinPath(basePath, "threads", string(r.threadID), "messages")
	if err != nil {
		return nil, nil, err
	}

	headerParams := make(openaiHeader)
	headerParams.AddContentTypeJSON().AddAssistantBeta()

	queryParams := url.Values{}
	r.page.Query(queryParams)

	req, err := e.client.prepareRequest(r.ctx, path, http.MethodGet, nil, headerParams, queryParams, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := e.client.callAPI(req)
	if err != nil {
		return nil, resp, nil
	}

	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp, nil
	}

	if resp.StatusCode >= 300 {
		return nil, resp, NewOpenAIAPIError(respBody, resp.Status, nil)
	}

	returnValue := new(PagedThreadMessages)
	if err = json.Unmarshal(respBody, returnValue); err != nil {
		return returnValue, resp, NewOpenAIAPIError(respBody, err.Error(), nil)
	}

	return returnValue, resp, nil
}
