package gogpt

type ModifyAssistantRequest struct {
	Model        Model     `json:"model,omitempty"`
	Name         *string   `json:"name,omitempty"`
	Description  *string   `json:"description,omitempty"`
	Instructions *string   `json:"instructions,omitempty"`
	Tools        []RunTool `json:"tools,omitempty"`
	FileIDs      []string  `json:"file_ids,omitempty"`
	Metadata     Metadata  `json:"metadata,omitempty"`
}

func NewModifyAssistantRequest(model Model, name *string, description *string, instructions *string, tools []RunTool, fileIDs []string, metadata Metadata) *ModifyAssistantRequest {
	return &ModifyAssistantRequest{
		Model:        model,
		Name:         name,
		Description:  description,
		Instructions: instructions,
		Tools:        tools,
		FileIDs:      fileIDs,
		Metadata:     metadata,
	}
}

type ModifyAssistantRequestBuilder struct {
	model        Model
	name         *string
	description  *string
	instructions *string
	tools        []RunTool
	fileIDs      []string
	metadata     Metadata
}

func NewModifyAssistantRequestBuilder(model Model) *ModifyAssistantRequestBuilder {
	return &ModifyAssistantRequestBuilder{model: model}
}

func (b *ModifyAssistantRequestBuilder) Build() *ModifyAssistantRequest {
	return &ModifyAssistantRequest{
		Model:        b.model,
		Name:         b.name,
		Description:  b.description,
		Instructions: b.instructions,
		Tools:        b.tools,
		FileIDs:      b.fileIDs,
		Metadata:     b.metadata,
	}
}

func (b *ModifyAssistantRequestBuilder) WithName(n string) *ModifyAssistantRequestBuilder {
	b.name = &n
	return b
}

func (b *ModifyAssistantRequestBuilder) WithDescription(d string) *ModifyAssistantRequestBuilder {
	b.description = &d
	return b
}

func (b *ModifyAssistantRequestBuilder) WithInstructions(i string) *ModifyAssistantRequestBuilder {
	b.instructions = &i
	return b
}

func (b *ModifyAssistantRequestBuilder) WithTools(t []RunTool) *ModifyAssistantRequestBuilder {
	b.tools = t
	return b
}

func (b *ModifyAssistantRequestBuilder) WithFileIDs(ids []string) *ModifyAssistantRequestBuilder {
	b.fileIDs = ids
	return b
}

func (b *ModifyAssistantRequestBuilder) WithMetadata(m Metadata) *ModifyAssistantRequestBuilder {
	b.metadata = m
	return b
}
