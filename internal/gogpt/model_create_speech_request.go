package gogpt

type CreateSpeechRequest struct {
	Model          Model                `json:"model"`
	Input          string               `json:"input"`
	Voice          Voice                `json:"voice"`
	ResponseFormat SpeechResponseFormat `json:"response_format,omitempty"`
	Speed          float64              `json:"speed,omitempty"`
}

func NewCreateSpeechRequest(model Model, input string, voice Voice) *CreateSpeechRequest {
	return &CreateSpeechRequest{Model: model, Input: input, Voice: voice}
}
