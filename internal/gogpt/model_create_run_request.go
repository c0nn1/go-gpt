package gogpt

type CreateRunRequest struct {
	AssistantID  AssistantID `json:"assistant_id"`
	Model        *string     `json:"model,omitempty"`
	Instructions *string     `json:"instructions,omitempty"`
	Tools        *[]RunTool  `json:"tools,omitempty"`
	Metadata     Metadata    `json:"metadata,omitempty"`
}

func NewCreateRunRequest(assistantID AssistantID, model *string, instructions *string, tools *[]RunTool, metadata Metadata) *CreateRunRequest {
	return &CreateRunRequest{
		AssistantID:  assistantID,
		Model:        model,
		Instructions: instructions,
		Tools:        tools,
		Metadata:     metadata,
	}
}
