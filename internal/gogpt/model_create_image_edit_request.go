package gogpt

type ImageEditResponseFormat string

const (
	ImageEditFormatURL     ImageEditResponseFormat = "url"
	ImageEditFormatB64JSON ImageEditResponseFormat = "b64_json"
)

type CreateImageEditRequest struct {
	FilePath       string
	Prompt         string
	MaskFilePath   string
	Model          Model
	N              *int
	Size           string
	ResponseFormat ImageEditResponseFormat
	User           string
}

func NewCreateImageEditRequest(imageFilePath, prompt string) *CreateImageEditRequest {
	return &CreateImageEditRequest{FilePath: imageFilePath, Prompt: prompt}
}
