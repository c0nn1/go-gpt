package gogpt

type AssistantFileID string

type AssistantFile struct {
	ID          AssistantFileID `json:"id"`
	Object      string          `json:"object"`
	CreatedAt   int             `json:"created_at"`
	AssistantID AssistantID     `json:"assistant_id"`
}
