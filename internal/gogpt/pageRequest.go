package gogpt

import (
	"net/url"
	"strconv"
)

type PageRequest struct {
	Limit  *int
	Order  *string
	After  *string
	Before *string
}

func NewPageRequestBuilder() *PageRequest {
	return &PageRequest{}
}

func (p *PageRequest) WithLimit(limit int) *PageRequest {
	p.Limit = &limit
	return p
}

func (p *PageRequest) WithOrder(order string) *PageRequest {
	p.Order = &order
	return p
}

func (p *PageRequest) WithAfter(after string) *PageRequest {
	p.After = &after
	return p
}

func (p *PageRequest) WithBefore(before string) *PageRequest {
	p.Before = &before
	return p
}

func (p *PageRequest) Query(v url.Values) {
	if p == nil {
		return
	}

	if p.Order != nil {
		v.Set("order", *p.Order)
	}
	if p.After != nil {
		v.Set("after", *p.After)
	}
	if p.Before != nil {
		v.Set("before", *p.Before)
	}
	if p.Limit != nil {
		v.Set("limit", strconv.Itoa(*p.Limit))
	}
}
