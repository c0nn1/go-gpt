FROM golang:1.21.5-bookworm as builder

WORKDIR /app

COPY go.mod go.sum ./

RUN go mod download

COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main ./cmd/discordbot

FROM alpine:latest

WORKDIR /root/

COPY --from=builder /app/main .

ENV APP_ENV=production
CMD ["./main"]
