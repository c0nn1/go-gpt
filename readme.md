## Few words first:
This project and idea are very young and in a very experimental state right now. Expect breaking changes and use at your own risk.

## Introduction
go-gpt is an open-source project that basically mimics ChatGPT.  
If you don't want to pay 20 bucks per month for ChatGPT Plus and are comfortable using Docker, this might be something for you.  
Check out the implemented [features](#Features) further below.  

Using the OpenAI web API, you're only billed for the API calls you make and the traffic you cause.  
Check out the pricing here: https://openai.com/pricing  
To get a feel for tokens, checkout the tokenizer: https://platform.openai.com/tokenizer  
When logged into your OpenAI account you can analyze your traffic here: https://platform.openai.com/usage

## So, how does this work exactly?  
The idea is to associate a Discord User ID with an OpenAI API token.  
When the associated user sends a message in a Discord thread, this application relays that message to the OpenAI API using the corresponding token. 
Right now this mapping is done via a configuration file.  
Since Discord messages are not encrypted, the app is designed to ensure that your API token remains hidden from Discord.  
The mapping feature enables you to allow trusted close ones to use your API token. However, be careful: any usage under your API key will count towards your traffic and billing, and you.  
Vice versa a trusted close one can provide you their API key, allowing you to associate it with their Discord User ID.

At this point, this project is not designed to support a large number of users per instance.  
Use it as a single user instance or share it with close and trusted people.

---
## Setup
### Preconditions: 
Make sure you have:
* Docker installed and running
* an OpenAI account and an API key (you can create a key here: https://platform.openai.com/api-keys) -> Make sure to store the key at a safe place during the creation process, as you will not get access to it again once it's created.
* set up a payment method if you plan to use OpenAI Plus features (like GPT-4)
* created a Discord Application (see [Setup Discord Application](#setup-discord-application))
* added the Discord Bot to your desired server (see [Setup Discord Application](#setup-discord-application))

### Docker Compose:
Create a `compose.yaml`:
```yaml
version: "3.8"

services:
  gogpt:
    image: registry.gitlab.com/c0nn1/go-gpt:vX-X-X
    restart: unless-stopped
    volumes:
      - ./data:/data
```

Running `docker compose up` should result in these messages:
```
starting go-gpt discord bot.
could not find find configuration file, created it here: "/data/config.toml"
websocket: close 4004: Authentication failed.
```

Run `docker compose down` and then open `config.toml` with write permissions.  
See [Configuration](#configuration) for guidance and explanation.  
Run `docker compose up` again.  
This time you should see messages like this:
```
starting go-gpt discord bot.
discord bot session opened
logged in as YourBotName#0000
bot is running
```

### Configuration:
A default `config.toml` looks like that

```toml
[auth]
app_id = 'app-id or client-id of discord application'
bot_token = 'the bot token'

[auth.user_id_token_map]
# map discord user ids to an open ai api tokens
'a-discord-user-id' = 'your-open-ai-api-token'
'another-user-id' = 'another-open-ai-token'

[discord]
# slash commands will be created in these servers
guild_ids = ['a guild (server) id', 'another guild id']

[conversation]
# how many recent messages will be included per request, to allow OpenAI to follow the conversation 
# mind the traffic, you pay for every included message, not only the new one.
include_prev_messages_count = 5
# after receiving the first user message in a thread, this will cause to ask OpenAI for a short thread title
# which will automatically replace the default title.
automatic_thread_title = true
# the default system message which is sent first on every request. the system message helps set the behavior of the assistant.
# you can find more information on this here: https://platform.openai.com/docs/guides/text-generation/chat-completions-api
system_message = 'You are a helpful assistant.'
# the default model to use (https://platform.openai.com/docs/api-reference/chat/create)
model = 'gpt-3.5-turbo-1106'
# the default value of max_tokens (https://platform.openai.com/docs/api-reference/chat/create)
max_tokens = 2000
```

---
## Setup Discord Application
* Head over to your Developer Portal at https://discord.com/developers/applications and click on "New Application".  
* Give it a nice sweet name.  
* Once created, click on "Bot" in the left menu and give it a nice sweet name as well.  
* On the same page, find "Privileged Gateway Intents" and activate "Message Content Intent". This enables the bot to read the content of messages. Without it, the bot will not be able to redirect any user message to OpenAI.

To create an invitation link, click on "OAuth2" in the left menu and click on "URL Generator".  
Tick the following options:

### Scopes
* `bot`

### Bot Permissions

Permissions:  
| Permission | Description |
|------------|-------------|
| Read Messages/View Channels | Allows the bot to redirect user messages to OpenAI |
| Create Private Threads | Allows the bot to create new private threads for new OpenAI conversations |
| Send Messages in Threads | Allows the bot to send messages containing the OpenAI responses |
| Manage Messages | Allows the bot to pin messages |
| Embed Links | Allows the bot to embed messages (required for conversation parameters) | 
| Read Message History | Allows the bot to include recent messages into the OpenAI conversation |

Done? Look at the bottom of the page. Under "Generated URL" is your invitation link.  
Copy that and paste it in your browser. Select the server whose ID you put into your `config.toml`.

---
## Features
### ChatCompletion
* use `/chat` to let the bot create a new private thread. This is the recommended way of starting conversations.
* When using the model `gpt-4-vision-preview` you can even upload images to your message and talk about it. (https://platform.openai.com/docs/guides/vision)
* use `/change-params` to change request parameters like `model`, `max_tokens` and more. You can see the current parameters in the pinned message of the private thread. The initial parameters are read from `config.toml`. 
* use `/speech` to generate audio from a given input text.
* use `/transcribe` to convert an audio file into text.
* use `/translate` to translate an audio file into English text.
* use `/imagine2` to generate images using DALL-E 2
* use `/imagine3` to generate images using DALL-E 3