package gptauth

type TokenStore struct {
	store map[string]string
}

func NewTokenStore(store map[string]string) *TokenStore {
	return &TokenStore{store: store}
}

func (ts *TokenStore) Token(userID string) (string, bool) {
	token, ok := ts.store[userID]
	if !ok {
		return "", false
	}

	return token, true
}
