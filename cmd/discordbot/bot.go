package main

import (
	"context"
	"fmt"
	"github.com/bwmarrin/discordgo"
	"go-gpt/cmd/discordbot/configuration"
	"go-gpt/cmd/discordbot/conversation"
	"go-gpt/cmd/discordbot/gptauth"
	"go-gpt/cmd/discordbot/slashcommands"
	"go-gpt/internal/gogpt"
	"log"
)

/*
Read Messages/View Channels
Create Private Threads
Send Messages in Threads
Manage Messages
Read Message History
*/

type Bot struct {
	cfg                 *configuration.BotConfiguration
	session             *discordgo.Session
	interactionHandlers map[string]slashcommands.CommandHandler
	apiClient           *gogpt.APIClient
	tokenStore          *gptauth.TokenStore
	conversationService *conversation.OpenAIConversationService
}

func NewBot(cfg *configuration.BotConfiguration, tokenStore *gptauth.TokenStore, gpt *gogpt.APIClient, conversationService *conversation.OpenAIConversationService) *Bot {
	return &Bot{
		cfg:                 cfg,
		apiClient:           gpt,
		tokenStore:          tokenStore,
		conversationService: conversationService,
	}
}

func (b *Bot) Start() error {
	var err error

	b.session, err = discordgo.New(fmt.Sprintf("Bot %s", b.cfg.BotToken))
	if err != nil {
		return err
	}

	b.session.AddHandler(b.handleReady)
	b.session.AddHandler(b.handleInteractionCreate)
	b.session.AddHandler(b.handleMessageCreate)

	if err = b.session.Open(); err != nil {
		return err
	}

	log.Println("discord bot session opened")

	commandBuilders := []slashcommands.CommandBuilder{
		slashcommands.NewChatCommandBuilder(),
		slashcommands.NewChangeChatParametersCommand(),
		slashcommands.NewSpeechCommandBuilder(),
		slashcommands.NewTranscribeCommandBuilder(),
		slashcommands.NewTranslateCommandBuilder(),
		slashcommands.NewImagine2CommandBuilder(),
		slashcommands.NewImagine3CommandBuilder(),
	}

	b.interactionHandlers = map[string]slashcommands.CommandHandler{
		slashcommands.CommandChat:                 slashcommands.NewChatCommandHandler(b.cfg.ConversationParameters),
		slashcommands.CommandChangeChatParameters: slashcommands.NewChangeChatParametersCommandHandler(b.cfg.ConversationParameters),
		slashcommands.CommandSpeech:               slashcommands.NewSpeechCommandHandler(b.apiClient),
		slashcommands.CommandTranscribe:           slashcommands.NewTranscribeCommandHandler(b.apiClient),
		slashcommands.CommandTranslate:            slashcommands.NewTranslateCommandHandler(b.apiClient),
		slashcommands.CommandImagine2:             slashcommands.NewImagine2CommandHandler(b.apiClient),
		slashcommands.CommandImagine3:             slashcommands.NewImagine3CommandHandler(b.apiClient),
	}

	if err = b.registerSlashCommands(commandBuilders); err != nil {
		return err
	}

	return nil
}

func (b *Bot) registerSlashCommands(commanders []slashcommands.CommandBuilder) error {
	appCommands := make([]*discordgo.ApplicationCommand, len(commanders))

	for i, c := range commanders {
		appCommands[i] = c.Build()
	}

	for _, g := range b.cfg.GuildIDs {
		if _, err := b.session.ApplicationCommandBulkOverwrite(b.cfg.AppID, g, appCommands); err != nil {
			return fmt.Errorf("failed to bulk overwrite application commands on guild %s: %v", g, err)
		}
	}

	return nil
}

func (b *Bot) handleReady(s *discordgo.Session, i *discordgo.Ready) {
	log.Printf("logged in as %s\n", s.State.User.String())
}

func (b *Bot) handleInteractionCreate(s *discordgo.Session, i *discordgo.InteractionCreate) {
	defer func() {
		if r := recover(); r != nil {
			log.Println("recovered in handleInteractionCreate", r)
		}
	}()

	if i.Type == discordgo.InteractionApplicationCommand {
		if err := b.handleApplicationCommand(s, i); err != nil {
			log.Println(err)
			return
		}
	}
}

func (b *Bot) handleApplicationCommand(s *discordgo.Session, i *discordgo.InteractionCreate) error {
	token, ok := b.tokenStore.Token(i.Member.User.ID)
	if !ok {
		if err := slashcommands.RespondInteraction(s, i).WithPredefinedResponse(slashcommands.InteractionResponseNoOpenAIToken); err != nil {
			return slashcommands.ErrFailedToAcknowledgeInteraction(err)
		}

		return nil
	}

	slashCommandName := i.ApplicationCommandData().Name
	handler, ok := b.interactionHandlers[slashCommandName]
	if !ok {
		return fmt.Errorf("no handler registered for slash command %s", slashCommandName)
	}

	ctx := context.WithValue(context.Background(), gogpt.ContextAccessToken, token)
	handlerContext := slashcommands.NewHandlerContext(s, i, ctx)
	return handler.Handle(handlerContext)
}

func (b *Bot) handleMessageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	defer func() {
		if r := recover(); r != nil {
			log.Println("recovered in handleMessageCreate")
		}
	}()

	// ignore messages from bots
	if m.Author.Bot {
		return
	}

	token, ok := b.tokenStore.Token(m.Author.ID)
	if !ok {
		log.Printf("user \"%s\" has no mapped OpenAI token. aborting.\n", m.Member.User.Mention())
		return
	}

	ch, err := s.Channel(m.ChannelID)
	if err != nil {
		log.Println(err)
		return
	}

	if ch.ThreadMetadata == nil {
		log.Println("ignoring messages sent outside a thread")
		return
	}

	c, err := b.conversationService.Conversation(s, m, token)
	if err != nil {
		log.Printf("failed to create new conversation: %v\n", err)
		return
	}

	go func() {
		if err := c.Start(); err != nil {
			log.Println(err)
		}
	}()
}

func (b *Bot) Stop() error {
	if b.session != nil {
		err := b.session.Close()
		return err
	}

	return nil
}
