package conversation

import (
	"context"
	"fmt"
	"github.com/bwmarrin/discordgo"
	"go-gpt/cmd/discordbot/configuration"
	"go-gpt/cmd/discordbot/gptauth"
	"go-gpt/internal/gogpt"
	"log"
	"strings"
)

// collection 30 chunks for a better experience in Discord-UI
// updating a message for every chunk causes a significant increase of execution time when calling ChannelMessageEdit
const (
	NewConversationTitle   = "New Chat"
	chunksPerMessageUpdate = 30
)

type OpenAIConversation struct {
	session *discordgo.Session
	message *discordgo.MessageCreate
	service *OpenAIConversationService
	ctx     context.Context
}

type OpenAIConversationService struct {
	conversationDefaults *configuration.ConversationParameters
	gpt                  *gogpt.APIClient
	tokenStore           *gptauth.TokenStore
}

func NewOpenAIConversationService(conversationDefaults *configuration.ConversationParameters, gpt *gogpt.APIClient) *OpenAIConversationService {
	return &OpenAIConversationService{conversationDefaults: conversationDefaults, gpt: gpt}
}

func (s *OpenAIConversationService) Conversation(session *discordgo.Session, m *discordgo.MessageCreate, openAIToken string) (*OpenAIConversation, error) {
	ctx := context.WithValue(context.Background(), gogpt.ContextAccessToken, openAIToken)
	return &OpenAIConversation{
		session: session,
		message: m,
		service: s,
		ctx:     ctx,
	}, nil
}

func (c *OpenAIConversation) Start() error {
	// ignore irrelevant message types
	if c.message.Type != discordgo.MessageTypeDefault {
		// don't start conversation on non-default messages
		return nil
	}

	// find pinned message with configuration embed
	pinnedConfigMessage, embed, err := FindPinnedConfigurationMessage(c.session, c.message.ChannelID)
	if err != nil {
		return fmt.Errorf("failed to find pinned configuration message: %v", err)
	}

	// decide which parameters to use
	var conversationParams *configuration.ConversationParameters
	if pinnedConfigMessage == nil {
		conversationParams = c.service.conversationDefaults
		if _, _, err = SendAndPinConfigurationMessage(c.session, c.message.ChannelID, conversationParams); err != nil {
			return fmt.Errorf("failed to send and pin configuration embed: %v", err)
		}
	} else {
		conversationParams, err = ConvertConfigurationEmbedToParameters(embed)
		if err != nil {
			return fmt.Errorf("failed to convert configuration embed to struct: %v", err)
		}
	}

	// build and send request -> process response chunks
	req, err := c.buildChatCompletionRequest(conversationParams)
	if err != nil {
		return fmt.Errorf("failed to build ChatCompletionRequest: %v", err)
	}

	chunks, _, err := c.service.gpt.ChatCompletionAPI.Send(c.ctx).SendRequest(req).Stream()
	if err != nil {
		return fmt.Errorf("failed to send ChatCompletionRequest: %v", err)
	}

	if conversationParams.AutomaticThreadTitle {
		go c.updateThreadTitleIfRequired(req.Messages[:2], conversationParams.Model)
	}

	if err = c.processChatCompletionChunks(chunks); err != nil {
		return fmt.Errorf("failed to proecess ChatCompletionChunks: %v", err)
	}

	return nil
}

func (c *OpenAIConversation) updateThreadTitleIfRequired(messages []gogpt.ConversationMessage, model gogpt.Model) {
	channel, err := c.session.Channel(c.message.ChannelID)
	if err != nil {
		log.Printf("failed to get channel: %v", err)
		return
	}

	if channel.Name == NewConversationTitle {
		if err = c.changeThreadName(messages, model); err != nil {
			log.Printf("failed to automatically change thread name: %v", err)
		}
	}
}

func (c *OpenAIConversation) buildChatCompletionRequest(params *configuration.ConversationParameters) (*gogpt.ChatCompletionRequest, error) {
	builder := gogpt.NewChatCompletionRequestBuilder(params.Model).
		WithStreaming(true).
		WithMaxTokens(params.MaxTokens).
		AddMessage(gogpt.NewSystemMessage(params.SystemMessage, nil))

	channelMessages, err := c.session.ChannelMessages(c.message.ChannelID, params.IncludePreviousMessagesCount, "", "", "")
	if err != nil {
		return nil, err
	}

	if len(channelMessages) == 0 {
		return builder.Build()
	}

	for i := len(channelMessages) - 1; i >= 0; i-- {
		channelMessage := channelMessages[i]

		if channelMessage.Type != discordgo.MessageTypeDefault {
			continue
		}

		if len(channelMessage.Embeds) == 1 && channelMessage.Embeds[0].Title == configurationEmbedTitle {
			continue
		}

		var message gogpt.ConversationMessage
		if channelMessage.Author.Bot {
			message = gogpt.AssistantMessage{
				BaseMessage: gogpt.BaseMessage{
					Content: channelMessage.Content,
					Role:    gogpt.MessageRoleAssistant,
				},
			}
		} else {
			messageParts := make([]gogpt.ArrayUserMessageContentPart, 0, len(channelMessage.Attachments)+1)
			messageParts = append(messageParts, gogpt.TextUserMessageContentPart{
				T:    "text",
				Text: channelMessage.Content,
			})

			for _, a := range channelMessage.Attachments {
				if strings.HasPrefix(a.ContentType, "image/") {
					messageParts = append(messageParts, gogpt.ImageUserMessageContentPart{
						T: "image_url",
						ImageURL: gogpt.ImageURL{
							URL:    a.URL,
							Detail: "auto",
						},
					})
				}
			}

			message = gogpt.UserMessage{
				Role:    gogpt.MessageRoleUser,
				Content: gogpt.ArrayUserMessageContent{Value: messageParts},
			}
		}

		builder.AddMessage(message)
	}

	return builder.Build()
}

func (c *OpenAIConversation) processChatCompletionChunks(chunks <-chan *gogpt.ChatCompletionChunkResponse) error {
	var msg *discordgo.Message
	var err error
	var partialMessage strings.Builder

	chunkCounter := 0
	for chunk := range chunks {
		chunkContent := chunk.Choices[0].Delta.Content
		if chunkContent.IsSet() {
			partialMessage.WriteString(*chunkContent.Get())
		}

		chunkCounter++
		if chunkCounter == chunksPerMessageUpdate {
			msg, err = c.createMessageOrAppendContent(msg, partialMessage.String())
			if err != nil {
				return err
			}

			partialMessage.Reset()
			chunkCounter = 0
		}
	}

	if chunkCounter > 0 {
		msg, err = c.createMessageOrAppendContent(msg, partialMessage.String())
		if err != nil {
			return err
		}
	}

	return nil
}

func (c *OpenAIConversation) createMessageOrAppendContent(existingMessage *discordgo.Message, nextContent string) (*discordgo.Message, error) {
	if existingMessage == nil {
		return c.session.ChannelMessageSend(c.message.ChannelID, nextContent)
	} else {
		var content strings.Builder
		content.WriteString(existingMessage.Content)
		content.WriteString(nextContent)
		return c.session.ChannelMessageEdit(c.message.ChannelID, existingMessage.ID, content.String())
	}
}

func (c *OpenAIConversation) changeThreadName(messagesToAnalyzeForTopic []gogpt.ConversationMessage, model gogpt.Model) error {
	threadName, err := c.requestThreadNameForConversation(model, messagesToAnalyzeForTopic)
	if err != nil {
		return fmt.Errorf("failed to request a thread name for the conversation: %v", err)
	}

	_, err = c.session.ChannelEdit(c.message.ChannelID, &discordgo.ChannelEdit{
		Name: *threadName,
	})

	if err != nil {
		return fmt.Errorf("failed to edit channel to set new thread name: %v", err)
	}

	return nil
}

func (c *OpenAIConversation) requestThreadNameForConversation(model gogpt.Model, messages []gogpt.ConversationMessage) (*string, error) {
	msg := gogpt.UserMessage{
		Role: gogpt.MessageRoleUser,
		Content: gogpt.ArrayUserMessageContent{Value: []gogpt.ArrayUserMessageContentPart{
			gogpt.TextUserMessageContentPart{
				T:    "text",
				Text: "Guess the topic of the users message and come up with a very short title for it. ideally use 3-6 words for the title. do only respond with the title. do not anything else to the message. use the users language.",
			},
		}},
	}

	req, err := gogpt.NewChatCompletionRequestBuilder(model).
		WithStreaming(false).
		WithMaxTokens(200).
		WithMessages(messages).
		AddMessage(msg).
		Build()

	resp, _, err := c.service.gpt.ChatCompletionAPI.Send(c.ctx).SendRequest(req).Execute()
	if err != nil {
		return nil, fmt.Errorf("failed to execute ChatCompletion request: %v", err)
	}

	return &resp.Choices[0].Message.Content, nil
}
