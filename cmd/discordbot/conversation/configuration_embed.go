package conversation

import (
	"fmt"
	"github.com/bwmarrin/discordgo"
	"github.com/pelletier/go-toml/v2"
	"go-gpt/cmd/discordbot/configuration"
)

const (
	configurationEmbedTitle = "Parameters for conversation"
)

func SendAndPinConfigurationMessage(session *discordgo.Session, channelID string, params *configuration.ConversationParameters) (*discordgo.Message, *discordgo.MessageEmbed, error) {
	embed, err := ConvertConfigurationParametersToEmbed(params)
	if err != nil {
		return nil, nil, fmt.Errorf("failed to convert parameters into embed: %v", err)
	}

	msg, err := session.ChannelMessageSendComplex(channelID, &discordgo.MessageSend{
		Embed: embed,
	})

	if err != nil {
		return nil, nil, fmt.Errorf("failed to send message to channel: %v", err)
	}

	if err = session.ChannelMessagePin(channelID, msg.ID); err != nil {
		return nil, nil, fmt.Errorf("failed to pin message: %v", err)
	}

	return msg, embed, nil
}

func ConvertConfigurationParametersToEmbed(params *configuration.ConversationParameters) (*discordgo.MessageEmbed, error) {
	tomlBytes, err := toml.Marshal(params)
	if err != nil {
		return nil, fmt.Errorf("failed to marshal parameters into toml: %v", err)
	}

	paramsMap := make(map[string]any)
	if err = toml.Unmarshal(tomlBytes, &paramsMap); err != nil {
		return nil, fmt.Errorf("failed to unmarshal toml into map: %v", err)
	}

	fields := make([]*discordgo.MessageEmbedField, 0, len(paramsMap))

	for k, v := range paramsMap {
		field := &discordgo.MessageEmbedField{
			Name:   k,
			Value:  fmt.Sprintf("%v", v),
			Inline: false,
		}
		fields = append(fields, field)
	}

	return &discordgo.MessageEmbed{
		Type:        discordgo.EmbedTypeRich,
		Title:       configurationEmbedTitle,
		Description: "These are the parameters used for this conversation. You can change them using slash commands.",
		Color:       183409,
		Fields:      fields,
	}, nil
}

func ConvertConfigurationEmbedToParameters(e *discordgo.MessageEmbed) (*configuration.ConversationParameters, error) {
	parameters := make(map[string]string, len(e.Fields))

	for _, f := range e.Fields {
		parameters[f.Name] = f.Value
	}

	return configuration.NewConversationParametersFromMap(parameters)
}

func FindPinnedConfigurationMessage(session *discordgo.Session, channelID string) (*discordgo.Message, *discordgo.MessageEmbed, error) {
	pinnedMessage, err := session.ChannelMessagesPinned(channelID)
	if err != nil {
		return nil, nil, fmt.Errorf("failed to get pinned messages: %v", err)
	}

	for _, pm := range pinnedMessage {
		if pm.Author.ID == session.State.User.ID && len(pm.Embeds) == 1 && pm.Embeds[0].Title == configurationEmbedTitle {
			return pm, pm.Embeds[0], nil
		}
	}

	return nil, nil, nil
}
