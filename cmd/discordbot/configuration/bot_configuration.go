package configuration

import (
	"fmt"
	"go-gpt/internal/gogpt"
	"reflect"
	"strconv"
	"strings"
)

type BotConfiguration struct {
	*AuthenticationConfiguration `toml:"auth"`
	*DiscordConfiguration        `toml:"discord"`
	*ConversationParameters      `toml:"conversation"`
}

type AuthenticationConfiguration struct {
	AppID    string `toml:"app_id"`
	BotToken string `toml:"bot_token"`
	// UserIDTokenMap maps Discord User IDs to OpenAI API tokens which are used for API requests
	UserIDTokenMap map[string]string `toml:"user_id_token_map"`
}

type DiscordConfiguration struct {
	GuildIDs []string `toml:"guild_ids"`
}

type ConversationParameters struct {
	// IncludePreviousMessagesCount defines the maximum amount of messages being included into a ChatCompletion request.
	IncludePreviousMessagesCount int `toml:"include_prev_messages_count,omitempty"`
	// AutomaticThreadTitle will send one more request to OpenAI to let OpenAI figure out a matching thread title.
	AutomaticThreadTitle bool `toml:"automatic_thread_title,omitempty"`
	// SystemMessage defines first message to be sent in a conversation
	SystemMessage string      `toml:"system_message,omitempty"`
	Model         gogpt.Model `toml:"model,omitempty"`
	MaxTokens     int         `toml:"max_tokens,omitempty"`
}

func NewConversationParametersFromMap(m map[string]string) (*ConversationParameters, error) {
	result := new(ConversationParameters)
	val := reflect.ValueOf(result).Elem()
	typ := val.Type()

	for i := 0; i < val.NumField(); i++ {
		field := val.Field(i)
		if !field.CanSet() {
			continue
		}
		tomlTag := typ.Field(i).Tag.Get("toml")
		if tomlTag == "" {
			continue
		}
		tomlKey := strings.Split(tomlTag, ",")[0] // Get the key without the `,omitempty`

		mapValue, ok := m[tomlKey]
		if !ok {
			continue
		}

		switch field.Kind() {
		case reflect.Bool:
			if boolValue, err := strconv.ParseBool(mapValue); err != nil {
				return nil, fmt.Errorf("failed to convert string into bool: %v", err)
			} else {
				field.SetBool(boolValue)
			}
		case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
			if intValue, err := strconv.ParseInt(mapValue, 10, 64); err != nil {
				return nil, fmt.Errorf("failed to convert string into int: %v", err)
			} else {
				field.SetInt(intValue)
			}
		case reflect.String:
			field.SetString(mapValue)
		default:
			return nil, fmt.Errorf("unsupported kind %s", field.Kind())
		}
	}

	return result, nil
}

func NewConfigurationScaffold() *BotConfiguration {
	return &BotConfiguration{
		AuthenticationConfiguration: &AuthenticationConfiguration{
			AppID:    "discord-app-id",
			BotToken: "discord-bot-token",
			UserIDTokenMap: map[string]string{
				"discord_user_id": "openai_api_token",
			},
		},
		DiscordConfiguration: &DiscordConfiguration{
			GuildIDs: []string{"guild_id_1", "guild_id_2"},
		},
		ConversationParameters: &ConversationParameters{
			IncludePreviousMessagesCount: 5,
			AutomaticThreadTitle:         true,
			SystemMessage:                "You are a helpful assistant.",
			Model:                        gogpt.ModelGPT4o,
			MaxTokens:                    2000,
		},
	}
}
