package main

import (
	"errors"
	"fmt"
	"github.com/pelletier/go-toml/v2"
	"go-gpt/cmd/discordbot/configuration"
	"go-gpt/cmd/discordbot/conversation"
	"go-gpt/cmd/discordbot/gptauth"
	"go-gpt/internal/gogpt"
	"io"
	"log"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"
)

const (
	dataDirectory  = "/data"
	configFilename = "config.toml"
)

const (
	envProduction  = "production"
	envDevelopment = "development"
)

const appEnvVar = "APP_ENV"

func main() {
	fmt.Println("starting go-gpt discord bot.")

	appEnv := os.Getenv(appEnvVar)
	if len(appEnv) == 0 || (appEnv != envDevelopment && appEnv != envProduction) {
		appEnv = envProduction
	}

	log.Printf("using environment %s\n", appEnv)

	var configurationFilepath string
	if appEnv == envProduction {
		configurationFilepath = filepath.Clean(filepath.Join(dataDirectory, configFilename))
	} else {
		wd, err := os.Getwd()
		if err != nil {
			log.Fatalf("failed to get working directory: %v", err)
		}
		configurationFilepath = filepath.Clean(filepath.Join(wd, "cmd", "discordbot", configFilename))
	}

	cfg, err := readOrCreateConfigurationFile(configurationFilepath)
	if err != nil {
		log.Fatal(err)
	}

	if err = sanityCheckConfiguration(cfg); err != nil {
		log.Fatalf("invalid configuration: %v", err)
	}

	tokenStore := gptauth.NewTokenStore(cfg.UserIDTokenMap)

	gptCfg := gogpt.NewConfiguration()
	gpt := gogpt.NewAPIClient(gptCfg)

	conversationService := conversation.NewOpenAIConversationService(cfg.ConversationParameters, gpt)

	bot := NewBot(cfg, tokenStore, gpt, conversationService)
	if err = bot.Start(); err != nil {
		log.Fatal(err)
	}

	log.Println("bot is running")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt)
	<-sc

	if err = bot.Stop(); err != nil {
		log.Fatal(err)
	}
}

func readOrCreateConfigurationFile(path string) (*configuration.BotConfiguration, error) {
	file, err := os.Open(path)
	if err != nil && os.IsNotExist(err) {
		if err = createConfigScaffold(path); err != nil {
			return nil, fmt.Errorf("failed to create configuration file here \"%s\": %v", path, err)
		}

		return nil, fmt.Errorf("could not find find configuration file, created it here: \"%s\"", path)
	} else if err != nil {
		return nil, fmt.Errorf("failed to open file \"%s\": %v", path, err)
	}

	configFileContent, err := io.ReadAll(file)
	if err != nil {
		return nil, fmt.Errorf("failed to read configuration file \"%s\": %v", path, err)
	}

	cfg := new(configuration.BotConfiguration)
	if err = toml.Unmarshal(configFileContent, cfg); err != nil {
		return nil, fmt.Errorf("failed to parse config file: %v", err)
	}

	return cfg, nil
}

func createConfigScaffold(path string) error {
	cfg := configuration.NewConfigurationScaffold()
	fileContent, err := toml.Marshal(cfg)
	if err != nil {
		return err
	}

	file, err := os.Create(path)
	if err != nil {
		return err
	}

	defer file.Close()

	_, err = file.Write(fileContent)
	return err
}

func sanityCheckConfiguration(cfg *configuration.BotConfiguration) error {
	if len(cfg.AppID) == 0 {
		return errors.New("AppID is empty")
	}

	if len(cfg.BotToken) == 0 {
		return errors.New("BotToken is empty")
	}

	if len(cfg.GuildIDs) == 0 {
		return errors.New("no guild IDs specified")
	}

	if len(cfg.UserIDTokenMap) == 0 {
		return errors.New("no user-token mapping specified")
	}

	if len(cfg.Model) == 0 {
		log.Printf("OpenAI model not specified, default to %s\n", gogpt.ModelGPT4o)
		cfg.Model = gogpt.ModelGPT4o
	}

	if cfg.IncludePreviousMessagesCount < 1 {
		log.Printf("invalid conversation max message count, default to 1")
		cfg.IncludePreviousMessagesCount = 1
	}

	if cfg.MaxTokens < 20 {
		log.Printf("invalid conversation max tokens, default to 20")
		cfg.MaxTokens = 20
	}

	if len(cfg.SystemMessage) == 0 {
		log.Printf("invalid value for system message, default to \"You are a helpful assistant.\"")
		cfg.SystemMessage = "You are a helpful assistant."
	}

	return nil
}
