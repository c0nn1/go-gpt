package slashcommands

import "github.com/bwmarrin/discordgo"

func RespondInteraction(s *discordgo.Session, i *discordgo.InteractionCreate) *InteractionAcknowledger {
	return NewInteractionAcknowledger(s, i)
}

type InteractionAcknowledger struct {
	s *discordgo.Session
	i *discordgo.InteractionCreate
}

func NewInteractionAcknowledger(s *discordgo.Session, i *discordgo.InteractionCreate) *InteractionAcknowledger {
	return &InteractionAcknowledger{s: s, i: i}
}

func (ack *InteractionAcknowledger) WithPredefinedResponse(response *discordgo.InteractionResponse) error {
	return ack.s.InteractionRespond(ack.i.Interaction, response)
}

func (ack *InteractionAcknowledger) WithSimpleFollowUp(message string) (*discordgo.Message, error) {
	return ack.s.InteractionResponseEdit(ack.i.Interaction, &discordgo.WebhookEdit{
		Content: &message,
	})
}

func (ack *InteractionAcknowledger) WithComplexFollowUp(edit *discordgo.WebhookEdit) (*discordgo.Message, error) {
	return ack.s.InteractionResponseEdit(ack.i.Interaction, edit)
}

var interactionAcknowledgeLoadingWithEphemeralFollowUp = &discordgo.InteractionResponse{
	Type: discordgo.InteractionResponseDeferredChannelMessageWithSource,
	Data: &discordgo.InteractionResponseData{
		TTS:   false,
		Flags: discordgo.MessageFlagsLoading | discordgo.MessageFlagsEphemeral,
	},
}

var interactionAcknowledgeLoadingWithFollowUp = &discordgo.InteractionResponse{
	Type: discordgo.InteractionResponseDeferredChannelMessageWithSource,
	Data: &discordgo.InteractionResponseData{
		TTS:   false,
		Flags: discordgo.MessageFlagsLoading,
	},
}

var InteractionResponseNoOpenAIToken = &discordgo.InteractionResponse{
	Type: discordgo.InteractionResponseChannelMessageWithSource,
	Data: &discordgo.InteractionResponseData{
		TTS:     false,
		Flags:   discordgo.MessageFlagsEphemeral,
		Content: "No OpenAI API token found",
	},
}
