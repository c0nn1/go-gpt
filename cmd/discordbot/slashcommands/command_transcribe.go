package slashcommands

import (
	"fmt"
	"github.com/bwmarrin/discordgo"
	"go-gpt/internal/gogpt"
)

type TranscribeCommandBuilder struct {
}

func NewTranscribeCommandBuilder() *TranscribeCommandBuilder {
	return &TranscribeCommandBuilder{}
}

func (b *TranscribeCommandBuilder) Build() *discordgo.ApplicationCommand {
	return &discordgo.ApplicationCommand{
		Type:                     discordgo.ChatApplicationCommand,
		Name:                     CommandTranscribe,
		NameLocalizations:        nil,
		DMPermission:             &dmPermission,
		NSFW:                     &nsfw,
		Description:              "Transcribes audio into the input language.",
		DescriptionLocalizations: nil,
		Options: []*discordgo.ApplicationCommandOption{
			b.buildFileOption(),
			b.buildLanguageOption(),
			b.buildPromptOption(),
			b.buildResponseFormatOption(),
			b.buildTemperatureOption(),
		},
	}
}

func (b *TranscribeCommandBuilder) buildFileOption() *discordgo.ApplicationCommandOption {
	return &discordgo.ApplicationCommandOption{
		Type:                     discordgo.ApplicationCommandOptionAttachment,
		Name:                     OptionTranscribeFile,
		NameLocalizations:        nil,
		Description:              "The audio file to transcribe. allowed formats: flac,mp3,mp4,mpeg,mpga,m4a,ogg,wav,webm",
		DescriptionLocalizations: nil,
		Required:                 true,
	}
}

func (b *TranscribeCommandBuilder) buildLanguageOption() *discordgo.ApplicationCommandOption {
	var minValue float64 = 2
	const maxValue float64 = 3

	return &discordgo.ApplicationCommandOption{
		Type:                     discordgo.ApplicationCommandOptionString,
		Name:                     OptionTranscribeLanguage,
		NameLocalizations:        nil,
		Description:              "The language of the audio input. In ISO-639-1 format.",
		DescriptionLocalizations: nil,
		Required:                 false,
		MinValue:                 &minValue,
		MaxValue:                 maxValue,
	}
}

func (b *TranscribeCommandBuilder) buildPromptOption() *discordgo.ApplicationCommandOption {
	return &discordgo.ApplicationCommandOption{
		Type:                     discordgo.ApplicationCommandOptionString,
		Name:                     OptionTranscribePrompt,
		NameLocalizations:        nil,
		Description:              "Guide the model's style or continue a previous audio segment. Should match the audio language.",
		DescriptionLocalizations: nil,
		Required:                 false,
	}
}

func (b *TranscribeCommandBuilder) buildResponseFormatOption() *discordgo.ApplicationCommandOption {
	choices := make([]*discordgo.ApplicationCommandOptionChoice, len(gogpt.TranscriptionResponseFormats))

	for i, v := range gogpt.TranscriptionResponseFormats {
		choices[i] = &discordgo.ApplicationCommandOptionChoice{
			Name:  string(v),
			Value: string(v),
		}
	}

	return &discordgo.ApplicationCommandOption{
		Type:                     discordgo.ApplicationCommandOptionString,
		Name:                     OptionTranscribeResponseFormat,
		NameLocalizations:        nil,
		Description:              "The format of the transcript output",
		DescriptionLocalizations: nil,
		Required:                 false,
		Choices:                  choices,
	}
}

func (b *TranscribeCommandBuilder) buildTemperatureOption() *discordgo.ApplicationCommandOption {
	minValue := 0.0
	const maxValue = 1.0

	return &discordgo.ApplicationCommandOption{
		Type:                     discordgo.ApplicationCommandOptionNumber,
		Name:                     OptionTranscribeTemperature,
		NameLocalizations:        nil,
		Description:              "The sampling temperature. Higher values make output more random.",
		DescriptionLocalizations: nil,
		Required:                 false,
		MinValue:                 &minValue,
		MaxValue:                 maxValue,
	}
}

type TranscribeCommandHandler struct {
	gpt *gogpt.APIClient
}

func NewTranscribeCommandHandler(gpt *gogpt.APIClient) *TranscribeCommandHandler {
	return &TranscribeCommandHandler{gpt: gpt}
}

func (h *TranscribeCommandHandler) Handle(ctx *HandlerContext) error {
	if err := RespondInteraction(ctx.s, ctx.i).WithPredefinedResponse(interactionAcknowledgeLoadingWithFollowUp); err != nil {
		return ErrFailedToAcknowledgeInteraction(err)
	}

	req := &gogpt.CreateTranscriptionRequest{
		Model:          gogpt.ModelWhisper1,
		ResponseFormat: gogpt.TranscriptionFormatText,
	}

	options := ctx.i.ApplicationCommandData().Options
	fileOption, ok := findOptionByName(options, OptionTranscribeFile)
	if ok {
		attachmentID := fileOption.Value.(string)
		var attachment *discordgo.MessageAttachment
		for _, a := range ctx.i.ApplicationCommandData().Resolved.Attachments {
			if a.ID == attachmentID {
				attachment = a
			}
		}

		if attachment == nil {
			return fmt.Errorf("failed to resolve attachment")
		}

		req.FilePath = attachment.URL
	} else {
		return ErrOptionNotProvided(OptionTranscribeFile)
	}

	languageOption, ok := findOptionByName(options, OptionTranscribeLanguage)
	if ok {
		req.Language = languageOption.StringValue()
	}

	promptOption, ok := findOptionByName(options, OptionTranscribePrompt)
	if ok {
		req.Prompt = promptOption.StringValue()
	}

	formatOption, ok := findOptionByName(options, OptionTranscribeResponseFormat)
	if ok {
		req.ResponseFormat = gogpt.TranscriptionResponseFormat(formatOption.StringValue())
	}

	temperatureOption, ok := findOptionByName(options, OptionTranscribeTemperature)
	if ok {
		req.Temperature = temperatureOption.FloatValue()
	}

	resp, _, err := h.gpt.AudioAPI.CreateTranscription(ctx.ctx).CreateTranscriptionRequest(req).Execute()
	if err != nil {
		return ErrOpenAIError(err)
	}

	if _, err = RespondInteraction(ctx.s, ctx.i).WithSimpleFollowUp(resp.Text); err != nil {
		return ErrFailedToAcknowledgeInteraction(err)
	}

	return nil
}
