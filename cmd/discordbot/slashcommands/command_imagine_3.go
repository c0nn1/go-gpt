package slashcommands

import (
	"github.com/bwmarrin/discordgo"
	"go-gpt/internal/gogpt"
)

type Imagine3CommandBuilder struct {
}

func NewImagine3CommandBuilder() *Imagine3CommandBuilder {
	return &Imagine3CommandBuilder{}
}

func (b *Imagine3CommandBuilder) Build() *discordgo.ApplicationCommand {
	return &discordgo.ApplicationCommand{
		Type:                     discordgo.ChatApplicationCommand,
		Name:                     CommandImagine3,
		NameLocalizations:        nil,
		DMPermission:             &dmPermission,
		NSFW:                     &nsfw,
		Description:              "Creates an image given a prompt using Dall-E 3.",
		DescriptionLocalizations: nil,
		Options: []*discordgo.ApplicationCommandOption{
			b.buildPromptOption(),
			b.buildQualityOption(),
			b.buildSizeOption(),
			b.buildStyleOption(),
		},
	}
}

func (b *Imagine3CommandBuilder) buildPromptOption() *discordgo.ApplicationCommandOption {
	const maxLength = 4000
	return &discordgo.ApplicationCommandOption{
		Type:              discordgo.ApplicationCommandOptionString,
		Name:              OptionImaginePrompt,
		NameLocalizations: nil,
		Description:       "A text description of the desired image(s).",
		Required:          true,
		MaxLength:         maxLength,
	}
}

func (b *Imagine3CommandBuilder) buildQualityOption() *discordgo.ApplicationCommandOption {
	choices := []*discordgo.ApplicationCommandOptionChoice{
		{
			Name:  string(gogpt.ImageQualityHD),
			Value: string(gogpt.ImageQualityHD),
		},
		{
			Name:  string(gogpt.ImageQualityStandard),
			Value: string(gogpt.ImageQualityStandard),
		},
	}

	return &discordgo.ApplicationCommandOption{
		Type:                     discordgo.ApplicationCommandOptionString,
		Name:                     OptionImagineQuality,
		NameLocalizations:        nil,
		Description:              "The quality of the image that will be generated.",
		DescriptionLocalizations: nil,
		Required:                 false,
		Choices:                  choices,
	}
}

func (b *Imagine3CommandBuilder) buildSizeOption() *discordgo.ApplicationCommandOption {
	choices := []*discordgo.ApplicationCommandOptionChoice{
		{
			Name:              string(gogpt.ImageSize1024),
			NameLocalizations: nil,
			Value:             string(gogpt.ImageSize1024),
		},
		{
			Name:              string(gogpt.ImageSize1792x1024),
			NameLocalizations: nil,
			Value:             string(gogpt.ImageSize1792x1024),
		},
		{
			Name:              string(gogpt.ImageSize1024x1792),
			NameLocalizations: nil,
			Value:             string(gogpt.ImageSize1024x1792),
		},
	}

	return &discordgo.ApplicationCommandOption{
		Type:              discordgo.ApplicationCommandOptionString,
		Name:              OptionImagineSize,
		NameLocalizations: nil,
		Description:       "The size of the generated images.",
		Required:          false,
		Choices:           choices,
	}
}

func (b *Imagine3CommandBuilder) buildStyleOption() *discordgo.ApplicationCommandOption {
	choices := []*discordgo.ApplicationCommandOptionChoice{
		{
			Name:              string(gogpt.ImageStyleNatural),
			NameLocalizations: nil,
			Value:             string(gogpt.ImageStyleNatural),
		},
		{
			Name:              string(gogpt.ImageStyleVivid),
			NameLocalizations: nil,
			Value:             string(gogpt.ImageStyleVivid),
		},
	}

	return &discordgo.ApplicationCommandOption{
		Type:              discordgo.ApplicationCommandOptionString,
		Name:              OptionImagineStyle,
		NameLocalizations: nil,
		Description:       "The style of the generated images.",
		Required:          false,
		Choices:           choices,
	}
}

type Imagine3CommandHandler struct {
	gpt *gogpt.APIClient
}

func NewImagine3CommandHandler(gpt *gogpt.APIClient) *Imagine3CommandHandler {
	return &Imagine3CommandHandler{gpt: gpt}
}

func (h *Imagine3CommandHandler) Handle(ctx *HandlerContext) error {
	if err := RespondInteraction(ctx.s, ctx.i).WithPredefinedResponse(interactionAcknowledgeLoadingWithFollowUp); err != nil {
		return ErrFailedToAcknowledgeInteraction(err)
	}

	n := 1
	urlFormat := gogpt.ImageFormatURL
	req := &gogpt.CreateImageRequest{
		Model:          gogpt.ModelDalle3,
		N:              &n,
		ResponseFormat: &urlFormat,
	}

	options := ctx.i.ApplicationCommandData().Options
	promptOption, ok := findOptionByName(options, OptionImaginePrompt)
	if ok {
		req.Prompt = promptOption.StringValue()
	} else {
		return ErrOptionNotProvided(OptionImaginePrompt)
	}

	qualityOption, ok := findOptionByName(options, OptionImagineQuality)
	if ok {
		req.Quality = gogpt.ImageQuality(qualityOption.StringValue())
	}

	sizeOption, ok := findOptionByName(options, OptionImagineSize)
	if ok {
		size := gogpt.ImageSize(sizeOption.StringValue())
		req.Size = &size
	}

	styleOption, ok := findOptionByName(options, OptionImagineStyle)
	if ok {
		style := gogpt.ImageStyle(styleOption.StringValue())
		req.Style = &style
	}

	resp, _, err := h.gpt.ImagesAPI.Create(ctx.ctx).CreateRequest(req).Execute()

	embeds := make([]*discordgo.MessageEmbed, len(resp.Data))
	for i, v := range resp.Data {
		embeds[i] = &discordgo.MessageEmbed{
			Image: &discordgo.MessageEmbedImage{
				URL: v.URL,
			},
			Type:  discordgo.EmbedTypeRich,
			Color: 183409,
		}
	}

	message := "Here are your images"
	_, err = RespondInteraction(ctx.s, ctx.i).WithComplexFollowUp(&discordgo.WebhookEdit{
		Content: &message,
		Embeds:  &embeds,
	})

	if err != nil {
		return ErrFailedToAcknowledgeInteraction(err)
	}

	return nil
}
