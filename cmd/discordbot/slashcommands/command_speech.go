package slashcommands

import (
	"bytes"
	"github.com/bwmarrin/discordgo"
	"go-gpt/internal/gogpt"
	"strings"
	"time"
)

type SpeechCommandBuilder struct {
}

func NewSpeechCommandBuilder() *SpeechCommandBuilder {
	return &SpeechCommandBuilder{}
}

func (b *SpeechCommandBuilder) Build() *discordgo.ApplicationCommand {
	return &discordgo.ApplicationCommand{
		Type:                     discordgo.ChatApplicationCommand,
		Name:                     CommandSpeech,
		NameLocalizations:        nil,
		NSFW:                     &nsfw,
		Description:              "Generates audio from the input text.",
		DescriptionLocalizations: nil,
		DMPermission:             &dmPermission,
		Options: []*discordgo.ApplicationCommandOption{
			b.buildVoiceModelOption(),
			b.buildInputOption(),
			b.buildVoiceOption(),
			b.buildResponseFormatOption(),
			b.buildSpeedOption(),
		},
	}
}

func (b *SpeechCommandBuilder) buildVoiceModelOption() *discordgo.ApplicationCommandOption {
	choices := make([]*discordgo.ApplicationCommandOptionChoice, len(gogpt.TextToSpeechModels))

	for i, v := range gogpt.TextToSpeechModels {
		choices[i] = &discordgo.ApplicationCommandOptionChoice{
			Name:  string(v),
			Value: string(v),
		}
	}

	return &discordgo.ApplicationCommandOption{
		Type:                     discordgo.ApplicationCommandOptionString,
		Name:                     OptionModel,
		NameLocalizations:        nil,
		Description:              "One of the available TTS models.",
		DescriptionLocalizations: nil,
		Required:                 true,
		Autocomplete:             false,
		Choices:                  choices,
	}
}

func (b *SpeechCommandBuilder) buildInputOption() *discordgo.ApplicationCommandOption {
	const maxLength = 4096
	return &discordgo.ApplicationCommandOption{
		Type:                     discordgo.ApplicationCommandOptionString,
		Name:                     OptionSpeechInput,
		NameLocalizations:        nil,
		Description:              "The text to generate audio for.",
		DescriptionLocalizations: nil,
		Required:                 true,
		Autocomplete:             false,
		MaxLength:                maxLength,
	}
}

func (b *SpeechCommandBuilder) buildVoiceOption() *discordgo.ApplicationCommandOption {
	choices := make([]*discordgo.ApplicationCommandOptionChoice, len(gogpt.Voices))

	for i, v := range gogpt.Voices {
		choices[i] = &discordgo.ApplicationCommandOptionChoice{
			Name:  string(v),
			Value: string(v),
		}
	}

	return &discordgo.ApplicationCommandOption{
		Type:                     discordgo.ApplicationCommandOptionString,
		Name:                     OptionSpeechVoice,
		NameLocalizations:        nil,
		Description:              "The voice to use generating audio.",
		DescriptionLocalizations: nil,
		Required:                 true,
		Choices:                  choices,
	}
}

func (b *SpeechCommandBuilder) buildResponseFormatOption() *discordgo.ApplicationCommandOption {
	choices := make([]*discordgo.ApplicationCommandOptionChoice, len(gogpt.SpeechResponseFormats))

	for i, v := range gogpt.SpeechResponseFormats {
		choices[i] = &discordgo.ApplicationCommandOptionChoice{
			Name:  string(v),
			Value: string(v),
		}
	}

	return &discordgo.ApplicationCommandOption{
		Type:                     discordgo.ApplicationCommandOptionString,
		Name:                     OptionSpeechResponseFormat,
		NameLocalizations:        nil,
		Description:              "The format to audio in.",
		DescriptionLocalizations: nil,
		Required:                 false,
		Choices:                  choices,
	}
}

func (b *SpeechCommandBuilder) buildSpeedOption() *discordgo.ApplicationCommandOption {
	minValue := 0.25
	const maxValue = 4.0

	return &discordgo.ApplicationCommandOption{
		Type:                     discordgo.ApplicationCommandOptionNumber,
		Name:                     OptionSpeechSpeed,
		NameLocalizations:        nil,
		Description:              "The speed of the generated audio.",
		DescriptionLocalizations: nil,
		Required:                 false,
		MinValue:                 &minValue,
		MaxValue:                 maxValue,
	}
}

type SpeechCommandHandler struct {
	gpt *gogpt.APIClient
}

func NewSpeechCommandHandler(gpt *gogpt.APIClient) *SpeechCommandHandler {
	return &SpeechCommandHandler{gpt: gpt}
}

func (h *SpeechCommandHandler) Handle(ctx *HandlerContext) error {
	if err := RespondInteraction(ctx.s, ctx.i).WithPredefinedResponse(interactionAcknowledgeLoadingWithFollowUp); err != nil {
		return ErrFailedToAcknowledgeInteraction(err)
	}

	req := &gogpt.CreateSpeechRequest{}

	options := ctx.i.ApplicationCommandData().Options
	modelOption, ok := findOptionByName(options, OptionModel)
	if ok {
		req.Model = gogpt.Model(modelOption.StringValue())
	} else {
		return ErrOptionNotProvided(OptionModel)
	}

	inputOption, ok := findOptionByName(options, OptionSpeechInput)
	if ok {
		req.Input = inputOption.StringValue()
	} else {
		return ErrOptionNotProvided(OptionSpeechInput)
	}

	voiceOption, ok := findOptionByName(options, OptionSpeechVoice)
	if ok {
		req.Voice = gogpt.Voice(voiceOption.StringValue())
	} else {
		return ErrOptionNotProvided(OptionSpeechVoice)
	}

	formatOption, ok := findOptionByName(options, OptionSpeechResponseFormat)
	if ok {
		req.ResponseFormat = gogpt.SpeechResponseFormat(formatOption.StringValue())
	}

	speedOption, ok := findOptionByName(options, OptionSpeechSpeed)
	if ok {
		req.Speed = speedOption.FloatValue()
	}

	resp, _, err := h.gpt.AudioAPI.CreateSpeech(ctx.ctx).CreateSpeechRequest(req).Execute()
	if err != nil {
		return ErrOpenAIError(err)
	}

	builder := strings.Builder{}
	builder.WriteString(time.Now().Format("2006-01-02_150405"))
	builder.WriteString("_voice")
	var contentType string
	switch req.ResponseFormat {
	case gogpt.SpeechFormatAAC:
		builder.WriteString(".acc")
		contentType = "audio/aac"
	case gogpt.SpeechFormatFLAC:
		builder.WriteString(".flac")
		contentType = "audio/flac"
	case gogpt.SpeechFormatOPUS:
		builder.WriteString(".opus")
		contentType = "audio/opus"
	case gogpt.SpeechFormatMP3:
		fallthrough
	default:
		builder.WriteString(".mp3")
		contentType = "audio/mp3"
	}

	buffer := bytes.NewBuffer(resp)
	message := "Here is your generated speech"
	_, err = RespondInteraction(ctx.s, ctx.i).WithComplexFollowUp(&discordgo.WebhookEdit{
		Content: &message,
		Files: []*discordgo.File{
			{
				Name:        builder.String(),
				ContentType: contentType,
				Reader:      buffer,
			},
		},
	})

	return nil
}
