package slashcommands

import "github.com/bwmarrin/discordgo"

type CommandBuilder interface {
	Build() *discordgo.ApplicationCommand
}

type CommandHandler interface {
	Handle(ctx *HandlerContext) error
}
