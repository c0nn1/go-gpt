package slashcommands

import (
	"github.com/bwmarrin/discordgo"
	"go-gpt/internal/gogpt"
	"math"
)

var (
	nsfw         = false
	dmPermission = false
)

func findOptionByName(options []*discordgo.ApplicationCommandInteractionDataOption, name string) (*discordgo.ApplicationCommandInteractionDataOption, bool) {
	for _, o := range options {
		if o.Name == name {
			return o, true
		}
	}

	return nil, false
}

func buildChatCompletionModelOption() *discordgo.ApplicationCommandOption {
	modelChoices := make([]*discordgo.ApplicationCommandOptionChoice, len(gogpt.ChatCompletionModels))

	for i, m := range gogpt.ChatCompletionModels {
		modelChoices[i] = &discordgo.ApplicationCommandOptionChoice{
			Name:  string(m),
			Value: string(m),
		}
	}

	return &discordgo.ApplicationCommandOption{
		Type:                     discordgo.ApplicationCommandOptionString,
		Name:                     OptionModel,
		NameLocalizations:        nil,
		Description:              "The OpenAI model to use for the conversation.",
		DescriptionLocalizations: nil,
		Required:                 false,
		Autocomplete:             false,
		Choices:                  modelChoices,
	}
}

func buildMaxTokensOption() *discordgo.ApplicationCommandOption {
	var minValue float64 = 20
	const maxValue float64 = math.MaxInt16

	return &discordgo.ApplicationCommandOption{
		Type:                     discordgo.ApplicationCommandOptionInteger,
		Name:                     OptionChatMaxTokens,
		NameLocalizations:        nil,
		Description:              "The maximum amount of tokens to generate for the response.",
		DescriptionLocalizations: nil,
		MinValue:                 &minValue,
		MaxValue:                 maxValue,
		Required:                 false,
		Autocomplete:             false,
	}
}

func buildAutomaticThreadTitleOption() *discordgo.ApplicationCommandOption {
	return &discordgo.ApplicationCommandOption{
		Type:                     discordgo.ApplicationCommandOptionBoolean,
		Name:                     OptionChatAutomaticThreadTitle,
		NameLocalizations:        nil,
		Description:              "Send an auto-prompt to OpenAI for the conversation's topic to update the thread title.",
		DescriptionLocalizations: nil,
		Required:                 false,
		Autocomplete:             false,
	}
}

func buildPreviousMessagesCountOption() *discordgo.ApplicationCommandOption {
	var minValue float64 = 1
	const maxValue float64 = 20

	return &discordgo.ApplicationCommandOption{
		Type:                     discordgo.ApplicationCommandOptionInteger,
		Name:                     OptionChatIncludePrevMessagesCount,
		NameLocalizations:        nil,
		Description:              "Adjust the count of prior messages included to enable OpenAI to track the ongoing chat.",
		DescriptionLocalizations: nil,
		MinValue:                 &minValue,
		MaxValue:                 maxValue,
		Required:                 false,
		Autocomplete:             false,
	}
}

func buildSystemMessageOption() *discordgo.ApplicationCommandOption {
	minLength := 0
	const maxLength = 250
	return &discordgo.ApplicationCommandOption{
		Type:                     discordgo.ApplicationCommandOptionString,
		Name:                     OptionChatSystemMessage,
		NameLocalizations:        nil,
		Description:              "Modify the system message for the conversation.",
		DescriptionLocalizations: nil,
		MinLength:                &minLength,
		MaxLength:                maxLength,
		Required:                 false,
		Autocomplete:             false,
	}

}
