package slashcommands

import (
	"fmt"
	"github.com/bwmarrin/discordgo"
	"go-gpt/cmd/discordbot/configuration"
	"go-gpt/cmd/discordbot/conversation"
	"go-gpt/internal/gogpt"
	"slices"
)

type ChangeChatParametersCommandBuilder struct {
}

func NewChangeChatParametersCommand() *ChangeChatParametersCommandBuilder {
	return &ChangeChatParametersCommandBuilder{}
}

func (c *ChangeChatParametersCommandBuilder) Build() *discordgo.ApplicationCommand {
	return &discordgo.ApplicationCommand{
		Type:                     discordgo.ChatApplicationCommand,
		Name:                     CommandChangeChatParameters,
		NameLocalizations:        nil,
		NSFW:                     &nsfw,
		Description:              "Allows you to change parameters for this chat.",
		DescriptionLocalizations: nil,
		DMPermission:             &dmPermission,
		Options:                  c.buildOptions(),
	}
}

func (c *ChangeChatParametersCommandBuilder) buildOptions() []*discordgo.ApplicationCommandOption {
	return []*discordgo.ApplicationCommandOption{
		buildChatCompletionModelOption(),
		buildMaxTokensOption(),
		buildAutomaticThreadTitleOption(),
		buildPreviousMessagesCountOption(),
		buildSystemMessageOption(),
	}
}

type ChangeChatParametersCommandHandler struct {
	conversationDefaults *configuration.ConversationParameters
}

func NewChangeChatParametersCommandHandler(defaults *configuration.ConversationParameters) *ChangeChatParametersCommandHandler {
	return &ChangeChatParametersCommandHandler{conversationDefaults: defaults}
}

func (h *ChangeChatParametersCommandHandler) Handle(ctx *HandlerContext) error {
	if err := RespondInteraction(ctx.s, ctx.i).WithPredefinedResponse(interactionAcknowledgeLoadingWithFollowUp); err != nil {
		return ErrFailedToAcknowledgeInteraction(err)
	}

	channel, err := ctx.s.Channel(ctx.i.ChannelID)
	if err != nil {
		return fmt.Errorf("failed to get channel: %v", err)
	}

	if channel.ThreadMetadata == nil {
		if _, err = RespondInteraction(ctx.s, ctx.i).WithSimpleFollowUp("Command is only allowed in threads."); err != nil {
			return fmt.Errorf("failed to respond with simple follow up: %v", err)
		}
		return nil
	}

	msg, embed, err := conversation.FindPinnedConfigurationMessage(ctx.s, ctx.i.ChannelID)
	if err != nil {
		return fmt.Errorf("failed to find pinned configuration message: %v", err)
	}

	if msg == nil {
		msg, embed, err = conversation.SendAndPinConfigurationMessage(ctx.s, ctx.i.ChannelID, h.conversationDefaults)
		if err != nil {
			return fmt.Errorf("failed to send and pin configuration message with default parameters: %v", err)
		}
	}

	params, err := conversation.ConvertConfigurationEmbedToParameters(embed)
	if err != nil {
		return fmt.Errorf("failed to convert configuration embed to parameters: %v", err)
	}

	options := ctx.i.ApplicationCommandData().Options

	modelOption, ok := findOptionByName(options, OptionModel)
	if ok {
		model := gogpt.Model(modelOption.StringValue())
		if !slices.Contains(gogpt.ChatCompletionModels, model) {
			return fmt.Errorf("%s is not a valid value", model)
		}

		params.Model = model
	}

	maxTokensOption, ok := findOptionByName(options, OptionChatMaxTokens)
	if ok {
		params.MaxTokens = int(maxTokensOption.IntValue())
	}

	automaticThreadTitleOption, ok := findOptionByName(options, OptionChatAutomaticThreadTitle)
	if ok {
		params.AutomaticThreadTitle = automaticThreadTitleOption.BoolValue()
	}

	includePrevMessageCountOption, ok := findOptionByName(options, OptionChatIncludePrevMessagesCount)
	if ok {
		params.IncludePreviousMessagesCount = int(includePrevMessageCountOption.IntValue())
	}

	systemMessageOption, ok := findOptionByName(options, OptionChatSystemMessage)
	if ok {
		params.SystemMessage = systemMessageOption.StringValue()
	}

	embed, err = conversation.ConvertConfigurationParametersToEmbed(params)
	if err != nil {
		return fmt.Errorf("failed to convert conversation parameters to embed: %v", err)
	}

	if _, err = ctx.s.ChannelMessageEditEmbed(ctx.i.ChannelID, msg.ID, embed); err != nil {
		return fmt.Errorf("failed to update embed in channel: %v", err)
	}

	if _, err = RespondInteraction(ctx.s, ctx.i).WithSimpleFollowUp("Parameters updated"); err != nil {
		return fmt.Errorf("failed to respond with follow up: %v", err)
	}
	return nil
}
