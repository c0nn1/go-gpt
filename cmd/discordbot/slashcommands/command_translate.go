package slashcommands

import (
	"fmt"
	"github.com/bwmarrin/discordgo"
	"go-gpt/internal/gogpt"
)

type TranslateCommandBuilder struct {
}

func NewTranslateCommandBuilder() *TranslateCommandBuilder {
	return &TranslateCommandBuilder{}
}

func (b *TranslateCommandBuilder) Build() *discordgo.ApplicationCommand {
	return &discordgo.ApplicationCommand{
		Type:                     discordgo.ChatApplicationCommand,
		Name:                     CommandTranslate,
		NameLocalizations:        nil,
		DMPermission:             &dmPermission,
		NSFW:                     &nsfw,
		Description:              "Translates audio into English.",
		DescriptionLocalizations: nil,
		Options: []*discordgo.ApplicationCommandOption{
			b.buildFileOption(),
			b.buildPromptOption(),
			b.buildResponseFormatOption(),
			b.buildTemperatureOption(),
		},
	}
}

func (b *TranslateCommandBuilder) buildFileOption() *discordgo.ApplicationCommandOption {
	return &discordgo.ApplicationCommandOption{
		Type:                     discordgo.ApplicationCommandOptionAttachment,
		Name:                     OptionTranslateFile,
		NameLocalizations:        nil,
		Description:              "The audio file to translate. allowed formats: flac, mp3, mp4, mpeg, mpga, m4a, ogg, wav, or webm",
		DescriptionLocalizations: nil,
		Required:                 true,
	}
}

func (b *TranslateCommandBuilder) buildPromptOption() *discordgo.ApplicationCommandOption {
	return &discordgo.ApplicationCommandOption{
		Type:                     discordgo.ApplicationCommandOptionString,
		Name:                     OptionTranslatePrompt,
		NameLocalizations:        nil,
		Description:              "Guide the model's style or continue a previous audio segment. The prompt should be in English.",
		DescriptionLocalizations: nil,
		Required:                 false,
	}
}

func (b *TranslateCommandBuilder) buildResponseFormatOption() *discordgo.ApplicationCommandOption {
	choices := make([]*discordgo.ApplicationCommandOptionChoice, len(gogpt.TranscriptionResponseFormats))

	for i, v := range gogpt.TranscriptionResponseFormats {
		choices[i] = &discordgo.ApplicationCommandOptionChoice{
			Name:  string(v),
			Value: string(v),
		}
	}

	return &discordgo.ApplicationCommandOption{
		Type:                     discordgo.ApplicationCommandOptionString,
		Name:                     OptionTranslateResponseFormat,
		NameLocalizations:        nil,
		Description:              "The format of the transcript output.",
		DescriptionLocalizations: nil,
		Required:                 false,
		Choices:                  choices,
	}
}

func (b *TranslateCommandBuilder) buildTemperatureOption() *discordgo.ApplicationCommandOption {
	minValue := 0.0
	const maxValue = 1.0

	return &discordgo.ApplicationCommandOption{
		Type:                     discordgo.ApplicationCommandOptionNumber,
		Name:                     OptionTranslateTemperature,
		NameLocalizations:        nil,
		Description:              "The sampling temperature. Higher values make output more random.",
		DescriptionLocalizations: nil,
		Required:                 false,
		MinValue:                 &minValue,
		MaxValue:                 maxValue,
	}
}

type TranslateCommandHandler struct {
	gpt *gogpt.APIClient
}

func NewTranslateCommandHandler(gpt *gogpt.APIClient) *TranslateCommandHandler {
	return &TranslateCommandHandler{gpt: gpt}
}

func (h *TranslateCommandHandler) Handle(ctx *HandlerContext) error {
	if err := RespondInteraction(ctx.s, ctx.i).WithPredefinedResponse(interactionAcknowledgeLoadingWithFollowUp); err != nil {
		return ErrFailedToAcknowledgeInteraction(err)
	}

	req := &gogpt.CreateTranslationRequest{
		Model:          gogpt.ModelWhisper1,
		ResponseFormat: gogpt.TranscriptionFormatText,
	}

	options := ctx.i.ApplicationCommandData().Options
	fileOption, ok := findOptionByName(options, OptionTranslateFile)
	if ok {
		attachmentID := fileOption.Value.(string)
		var attachment *discordgo.MessageAttachment
		for _, a := range ctx.i.ApplicationCommandData().Resolved.Attachments {
			if a.ID == attachmentID {
				attachment = a
			}
		}

		if attachment == nil {
			return fmt.Errorf("failed to resolve attachment")
		}

		req.FilePath = attachment.URL
	} else {
		return ErrOptionNotProvided(OptionTranslateFile)
	}

	promptOption, ok := findOptionByName(options, OptionTranslatePrompt)
	if ok {
		req.Prompt = promptOption.StringValue()
	}

	formatOption, ok := findOptionByName(options, OptionTranslateResponseFormat)
	if ok {
		req.ResponseFormat = gogpt.TranscriptionResponseFormat(formatOption.StringValue())
	}

	temperatureOption, ok := findOptionByName(options, OptionTranslateTemperature)
	if ok {
		req.Temperature = temperatureOption.FloatValue()
	}

	resp, _, err := h.gpt.AudioAPI.CreateTranslation(ctx.ctx).CreateTranslationRequest(req).Execute()
	if err != nil {
		return ErrOpenAIError(err)
	}

	if _, err = RespondInteraction(ctx.s, ctx.i).WithSimpleFollowUp(resp.Text); err != nil {
		return ErrFailedToAcknowledgeInteraction(err)
	}

	return nil
}
