package slashcommands

import "fmt"

func ErrFailedToAcknowledgeInteraction(err error) error {
	return fmt.Errorf("failed to acknowledge interaction: %v", err)
}

func ErrOptionNotProvided(optionName string) error {
	return fmt.Errorf("option %s was not provided", optionName)
}

func ErrOpenAIError(err error) error {
	return fmt.Errorf("failed to execute OpenAI request: %v", err)
}

func ErrNoOpenAIToken(userID string) error {
	return fmt.Errorf("user with ID %s has no associated OpenAI API token", userID)
}
