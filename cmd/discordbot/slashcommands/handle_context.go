package slashcommands

import (
	"context"
	"github.com/bwmarrin/discordgo"
)

type HandlerContext struct {
	s   *discordgo.Session
	i   *discordgo.InteractionCreate
	ctx context.Context
}

func NewHandlerContext(s *discordgo.Session, i *discordgo.InteractionCreate, ctx context.Context) *HandlerContext {
	return &HandlerContext{s: s, i: i, ctx: ctx}
}
