package slashcommands

import (
	"fmt"
	"github.com/bwmarrin/discordgo"
	"go-gpt/cmd/discordbot/configuration"
	"go-gpt/cmd/discordbot/conversation"
	"go-gpt/internal/gogpt"
	"slices"
	"time"
)

type ChatCommandBuilder struct {
}

func NewChatCommandBuilder() *ChatCommandBuilder {
	return &ChatCommandBuilder{}
}

func (cmd *ChatCommandBuilder) Build() *discordgo.ApplicationCommand {
	return &discordgo.ApplicationCommand{
		Type:                     discordgo.ChatApplicationCommand,
		Name:                     CommandChat,
		NameLocalizations:        nil,
		NSFW:                     &nsfw,
		Description:              "Creates a private tread to start a OpenAI chat.",
		DescriptionLocalizations: nil,
		DMPermission:             &dmPermission,
		Options:                  cmd.buildOptions(),
	}
}

func (cmd *ChatCommandBuilder) buildOptions() []*discordgo.ApplicationCommandOption {
	return []*discordgo.ApplicationCommandOption{
		buildChatCompletionModelOption(),
		buildMaxTokensOption(),
		buildAutomaticThreadTitleOption(),
		buildPreviousMessagesCountOption(),
		buildSystemMessageOption(),
	}
}

type ChatCommandHandler struct {
	conversationDefaults *configuration.ConversationParameters
}

func NewChatCommandHandler(defaults *configuration.ConversationParameters) *ChatCommandHandler {
	return &ChatCommandHandler{conversationDefaults: defaults}
}

func (h *ChatCommandHandler) Handle(ctx *HandlerContext) error {
	if err := RespondInteraction(ctx.s, ctx.i).WithPredefinedResponse(interactionAcknowledgeLoadingWithFollowUp); err != nil {
		return ErrFailedToAcknowledgeInteraction(err)
	}

	inactivity, _ := time.ParseDuration("24h")
	channel, err := ctx.s.ThreadStart(ctx.i.ChannelID, conversation.NewConversationTitle, discordgo.ChannelTypeGuildPrivateThread, int(inactivity.Minutes()))
	if err != nil {
		return fmt.Errorf("failed to start thread: %v", err)
	}

	msg := fmt.Sprintf("New thread created here: %s", channel.Mention())
	if _, err = RespondInteraction(ctx.s, ctx.i).WithSimpleFollowUp(msg); err != nil {
		return fmt.Errorf("failed to respond with follow up: %v", err)
	}

	params := h.conversationDefaults
	options := ctx.i.ApplicationCommandData().Options

	modelOption, ok := findOptionByName(options, OptionModel)
	if ok {
		model := gogpt.Model(modelOption.StringValue())
		if !slices.Contains(gogpt.ChatCompletionModels, model) {
			return fmt.Errorf("%s is not a valid value", model)
		}

		params.Model = model
	}

	maxTokensOption, ok := findOptionByName(options, OptionChatMaxTokens)
	if ok {
		params.MaxTokens = int(maxTokensOption.IntValue())
	}

	automaticThreadTitleOption, ok := findOptionByName(options, OptionChatAutomaticThreadTitle)
	if ok {
		params.AutomaticThreadTitle = automaticThreadTitleOption.BoolValue()
	}

	includePrevMessagesCountOption, ok := findOptionByName(options, OptionChatIncludePrevMessagesCount)
	if ok {
		params.IncludePreviousMessagesCount = int(includePrevMessagesCountOption.IntValue())
	}

	systemMessageOption, ok := findOptionByName(options, OptionChatSystemMessage)
	if ok {
		params.SystemMessage = systemMessageOption.StringValue()
	}

	if _, _, err = conversation.SendAndPinConfigurationMessage(ctx.s, channel.ID, params); err != nil {
		return fmt.Errorf("failed to send and pin configuration message: %v", err)
	}

	return nil
}
