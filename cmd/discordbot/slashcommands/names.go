package slashcommands

const (
	CommandChat                 = "chat"
	CommandChangeChatParameters = "change-params"
	CommandSpeech               = "speech"
	CommandTranscribe           = "transcribe"
	CommandTranslate            = "translate"
	CommandImagine2             = "imagine2"
	CommandImagine3             = "imagine3"
)

const (
	OptionModel = "model"

	OptionChatMaxTokens                = "max-tokens"
	OptionChatAutomaticThreadTitle     = "automatic-thread-title"
	OptionChatIncludePrevMessagesCount = "include-prev-messages-count"
	OptionChatSystemMessage            = "system-message"

	OptionSpeechInput          = "input"
	OptionSpeechVoice          = "voice"
	OptionSpeechResponseFormat = "response-format"
	OptionSpeechSpeed          = "speed"

	OptionTranscribeFile           = "file"
	OptionTranscribeLanguage       = "language"
	OptionTranscribePrompt         = "prompt"
	OptionTranscribeResponseFormat = "response-format"
	OptionTranscribeTemperature    = "temperature"

	OptionTranslateFile           = "file"
	OptionTranslatePrompt         = "prompt"
	OptionTranslateResponseFormat = "response-format"
	OptionTranslateTemperature    = "temperature"

	OptionImaginePrompt  = "prompt"
	OptionImagineNumber  = "number"
	OptionImagineQuality = "quality"
	OptionImagineSize    = "size"
	OptionImagineStyle   = "style"
)
