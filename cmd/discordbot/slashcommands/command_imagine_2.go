package slashcommands

import (
	"github.com/bwmarrin/discordgo"
	"go-gpt/internal/gogpt"
)

type Imagine2CommandBuilder struct {
}

func NewImagine2CommandBuilder() *Imagine2CommandBuilder {
	return &Imagine2CommandBuilder{}
}

func (b *Imagine2CommandBuilder) Build() *discordgo.ApplicationCommand {
	return &discordgo.ApplicationCommand{
		Type:                     discordgo.ChatApplicationCommand,
		Name:                     CommandImagine2,
		NameLocalizations:        nil,
		DMPermission:             &dmPermission,
		NSFW:                     &nsfw,
		Description:              "Creates an image given a prompt using Dall-E 2.",
		DescriptionLocalizations: nil,
		Options: []*discordgo.ApplicationCommandOption{
			b.buildPromptOption(),
			b.buildNumberOption(),
			b.buildSizeOption(),
		},
	}
}

func (b *Imagine2CommandBuilder) buildPromptOption() *discordgo.ApplicationCommandOption {
	const maxLength = 1000
	return &discordgo.ApplicationCommandOption{
		Type:              discordgo.ApplicationCommandOptionString,
		Name:              OptionImaginePrompt,
		NameLocalizations: nil,
		Description:       "A text description of the desired image(s).",
		Required:          true,
		MaxLength:         maxLength,
	}
}

func (b *Imagine2CommandBuilder) buildNumberOption() *discordgo.ApplicationCommandOption {
	var minValue float64 = 1
	const maxValue = 10

	return &discordgo.ApplicationCommandOption{
		Type:                     discordgo.ApplicationCommandOptionInteger,
		Name:                     OptionImagineNumber,
		NameLocalizations:        nil,
		Description:              "The number of images to create.",
		DescriptionLocalizations: nil,
		Required:                 false,
		MinValue:                 &minValue,
		MaxValue:                 maxValue,
	}
}

func (b *Imagine2CommandBuilder) buildSizeOption() *discordgo.ApplicationCommandOption {
	choices := []*discordgo.ApplicationCommandOptionChoice{
		{
			Name:              string(gogpt.ImageSize256),
			NameLocalizations: nil,
			Value:             string(gogpt.ImageSize256),
		},
		{
			Name:              string(gogpt.ImageSize512),
			NameLocalizations: nil,
			Value:             string(gogpt.ImageSize512),
		},
		{
			Name:              string(gogpt.ImageSize1024),
			NameLocalizations: nil,
			Value:             string(gogpt.ImageSize1024),
		},
	}

	return &discordgo.ApplicationCommandOption{
		Type:              discordgo.ApplicationCommandOptionString,
		Name:              OptionImagineSize,
		NameLocalizations: nil,
		Description:       "The size of the generated images.",
		Required:          false,
		Choices:           choices,
	}
}

type Imagine2CommandHandler struct {
	gpt *gogpt.APIClient
}

func NewImagine2CommandHandler(gpt *gogpt.APIClient) *Imagine2CommandHandler {
	return &Imagine2CommandHandler{gpt: gpt}
}

func (h *Imagine2CommandHandler) Handle(ctx *HandlerContext) error {
	if err := RespondInteraction(ctx.s, ctx.i).WithPredefinedResponse(interactionAcknowledgeLoadingWithFollowUp); err != nil {
		return ErrFailedToAcknowledgeInteraction(err)
	}

	urlFormat := gogpt.ImageFormatURL
	req := &gogpt.CreateImageRequest{
		Model:          gogpt.ModelDalle2,
		ResponseFormat: &urlFormat,
	}

	options := ctx.i.ApplicationCommandData().Options
	promptOption, ok := findOptionByName(options, OptionImaginePrompt)
	if ok {
		req.Prompt = promptOption.StringValue()
	} else {
		return ErrOptionNotProvided(OptionImaginePrompt)
	}

	numberOption, ok := findOptionByName(options, OptionImagineNumber)
	if ok {
		n := int(numberOption.IntValue())
		req.N = &n
	}

	sizeOption, ok := findOptionByName(options, OptionImagineSize)
	if ok {
		size := gogpt.ImageSize(sizeOption.StringValue())
		req.Size = &size
	}

	resp, _, err := h.gpt.ImagesAPI.Create(ctx.ctx).CreateRequest(req).Execute()
	if err != nil {
		return ErrOpenAIError(err)
	}

	embeds := make([]*discordgo.MessageEmbed, len(resp.Data))
	for i, v := range resp.Data {
		embeds[i] = &discordgo.MessageEmbed{
			Image: &discordgo.MessageEmbedImage{
				URL: v.URL,
			},
			Type:  discordgo.EmbedTypeRich,
			Color: 183409,
		}
	}

	message := "Here are your images"
	_, err = RespondInteraction(ctx.s, ctx.i).WithComplexFollowUp(&discordgo.WebhookEdit{
		Content: &message,
		Embeds:  &embeds,
	})

	if err != nil {
		return ErrFailedToAcknowledgeInteraction(err)
	}

	return nil
}
