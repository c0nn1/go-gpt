{
    description = "A Nix-flake-based Go 1.22 development environment";

    inputs.nixpkgs.url = "github:NixOS/nixpkgs/24.05";

    outputs = { self, nixpkgs }:
        let
            goMinorVersion = 22;
            overlays = [ (final: prev: { go = prev."go_1_${toString goMinorVersion}"; }) ];
            supportedSystems = [ "x86_64-linux" "aarch64-linux" "x86_64-darwin" "aarch64-darwin" ];
            forEachSupportedSystem = f: nixpkgs.lib.genAttrs supportedSystems (system: f {
                pkgs = import nixpkgs { inherit overlays system; };
            });
        in
        {
            devShells = forEachSupportedSystem({ pkgs }: {
                default = pkgs.mkShell {
                    packages = with pkgs; [
                        go_1_22
                        gotools
                        golangci-lint
                        gopls
                    ];
                };
            });
        };
}
